package br.com.afazenda.controller;

import br.com.afazenda.model.vo.ItemProduto;
import br.com.afazenda.model.vo.Produto;
import br.com.afazenda.model.vo.Venda;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class VendaTest {

    private static Venda venda;
    private static ArrayList<ItemProduto> itensProdutos = new ArrayList<>();

    @BeforeClass
    public static void inicializarValores() {
        Produto p1 = new Produto(0, 10, "kg", 100, "Pão", 'v', "12/12/2009");
        Produto p2 = new Produto(1, 12, "kg", 200, "Arroz", 'v', "12/12/2017");
        Produto p3 = new Produto(2, 5, "l", 200, "Suco", 'v', "11/10/2017");

        itensProdutos.add(new ItemProduto(10, p1));
        itensProdutos.add(new ItemProduto(3, p2));
        itensProdutos.add(new ItemProduto(1, p3));

        venda = new Venda(itensProdutos, null, null, 0);
    }

    @Test
    public void calcularTroco() {
        //valor total 141
        Venda v = new Venda(itensProdutos, null, null, 200);

        assertEquals(59, v.getTroco(), 0.02);
    }

    /**
     * Testa o valor da venda somando todos os itens
     */
    @Test
    public void calcularValorVenda() {
        //10 * 10 + 3 * 12 + 5 * 1
        assertEquals(141, venda.getValorVenda(), 0.02);
    }

}
