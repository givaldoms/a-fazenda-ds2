package br.com.afazenda.controller;

import br.com.afazenda.model.vo.Cargo;
import br.com.afazenda.model.vo.Funcionario;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;

import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;

/**
 * Teste caixa branca utilizando Mockito
 */
public class CadastroContratoTest {

    private static Funcionario funcionario;
    private static Cargo cargo;

    @Mock
    private CadastroContratoController cadastroContrato = new CadastroContratoController();

    public CadastroContratoTest() throws SQLException {
    }

    @BeforeClass
    public static void inicializar() {
        funcionario = new Funcionario("111.111.111-11", "Gabriel Souza", "(79)97877727", "gabriel@gmail@com", 0, "10/20/90", true);
        cargo = new Cargo();
        cargo.setId(1);
        cargo.setNome("cargo 1");
    }


    @Test
    public void cadastroOk() throws Exception {
        String dataInicio = "20/08/10";
        String dataFim = "30/12/20";
        String salario = "2000.40";
        assertNotNull(cadastroContrato.criarContrato(funcionario.getCpf(), salario, dataInicio, dataFim, cargo));
    }


}