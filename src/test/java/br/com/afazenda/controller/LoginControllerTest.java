package br.com.afazenda.controller;

import br.com.afazenda.model.interfacedao.FuncionarioDao;
import br.com.afazenda.model.vo.Funcionario;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;

import java.sql.SQLException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Teste caixa preta utilizando Mockito
 */
public class LoginControllerTest {
    private static Funcionario funcionario;
    @Mock
    private static FuncionarioDao mockedFuncionario;
    private LoginController login = new LoginController();

    public LoginControllerTest() throws SQLException {
    }

    @BeforeClass
    public static void inicializarCliente() {
        mockedFuncionario = mock(FuncionarioDao.class);
        funcionario = new Funcionario("111.111.111-11", "Gabriel Souza", "(79)97877727", "gabriel@gmail.com", 0, "10/20/1990", true);
        //Funcionario f2 = new Funcionario("054.020.321-11", "Felipe Bastardo", "(79)22222222", "felipox@ufs.br", 1, "09/11/2003", false);
        //Funcionario f3 = new Funcionario("03302032111", "Lorena Louraine", "(79)00001111", "lorena@dcomp.ufs.br", 2, "09/11/1982", true);

    }

    @Test
    public void logarAtivo() throws Exception {
        String cpfTemp = "111.111.111-11";
        Funcionario f = new Funcionario(cpfTemp, "Gabriel Souza", "(79)97877727", "gabriel@gmail.com", 0, "10/20/90", true);
        when(mockedFuncionario.buscarFuncionario(funcionario.getCpf())).thenReturn(f);
        assertTrue("True", login.fazerLogin(mockedFuncionario, cpfTemp));

    }

    @Test
    public void logarInativo() throws Exception {
        Funcionario f = funcionario;
        f.setAtivo(false);
        when(mockedFuncionario.buscarFuncionario(funcionario.getCpf())).thenReturn(f);
        String cpfTemp = "054.020.321-11";
        assertFalse("false", login.fazerLogin(mockedFuncionario, cpfTemp));

    }

    @Test
    public void logarInvalido() throws Exception {
        String cpfTemp = "03302032111";
        Funcionario f = new Funcionario(cpfTemp, "Lorena Louraine", "(79)00001111", "lorena@dcomp.ufs.br", 2, "09/11/1982", true);
        when(mockedFuncionario.buscarFuncionario(funcionario.getCpf())).thenReturn(f);
        assertFalse("false", login.fazerLogin(mockedFuncionario, cpfTemp));

    }

    @Test
    public void logarInexistenteValido() throws Exception {
        String cpfTemp = "054.020.222-11";
        when(mockedFuncionario.buscarFuncionario(funcionario.getCpf())).thenReturn(null);
        assertFalse("false", login.fazerLogin(mockedFuncionario, cpfTemp));


    }

    @Test
    public void logarInexistenteInvalido() throws Exception {
        String cpfTemp = "054.0200000011";
        when(mockedFuncionario.buscarFuncionario(funcionario.getCpf())).thenReturn(null);
        assertFalse("false", login.fazerLogin(mockedFuncionario, cpfTemp));


    }


}