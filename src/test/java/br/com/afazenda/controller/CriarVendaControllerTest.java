package br.com.afazenda.controller;

import br.com.afazenda.model.interfacedao.ClienteDao;
import br.com.afazenda.model.vo.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Teste caixa branca utilizando Mockito
 */
public class CriarVendaControllerTest {

    private static Cliente cliente;
    private static Venda venda;
    private static Funcionario funcionario;
    private static ArrayList<ItemProduto> itensProdutos = new ArrayList<>();

    @Mock
    private static ClienteDao mockedCliente;

    private CriarVendaController criarVenda = new CriarVendaController();

    public CriarVendaControllerTest() throws SQLException {
    }

    @BeforeClass
    public static void inicializarCliente() {
        Produto p1 = new Produto(0, 10, "kg", 100, "Pão", 'v', "12/12/2009");
        Produto p2 = new Produto(1, 12, "kg", 200, "Arroz", 'v', "12/12/2017");
        Produto p3 = new Produto(2, 5, "l", 200, "Suco", 'v', "11/10/2017");

        itensProdutos.add(new ItemProduto(10, p1));
        itensProdutos.add(new ItemProduto(3, p2));
        itensProdutos.add(new ItemProduto(1, p3));

        mockedCliente = mock(ClienteDao.class);

        funcionario = new Funcionario("111.111.111-11", "Gabriel Souza", "(79)97877727", "gabriel@gmail@com", 0, "10/20/1990", true);
        cliente = new Cliente("111.111.111-22", "Givaldo Marques", "(79)981583468", "junioraqw@gmail.com", 0, false);
        venda = new Venda(itensProdutos, funcionario, cliente, 0);
    }


    /**
     * Caso válido
     */
    @Test
    public void salvarVendaValido() throws Exception {

        String valorPago = "200";//campo de texto
        when(mockedCliente.buscarCliente(cliente.getCpf())).thenReturn(cliente);
        assertNotNull(criarVenda.salvarVenda(mockedCliente, valorPago, venda, cliente.getCpf()));
    }

    /**
     * Salvar vendas com quantidade de itens inválidos
     */
    @Test
    public void salvarVenda0Itens() throws Exception {
        String valorPago = String.valueOf(venda.getValorVenda() + 1);//campo de texto
        Venda v = new Venda(new ArrayList<>(), funcionario, cliente, 0);
        when(mockedCliente.buscarCliente(cliente.getCpf())).thenReturn(cliente);

        v.setItens(new ArrayList<>());//lista vazia: 0 itens
        assertNull(criarVenda.salvarVenda(mockedCliente, valorPago, v, cliente.getCpf()));

    }

    /**
     * Salvar vendas com cliente devedor
     */

    @Test
    public void salvarVendaClinteInadinplente() throws Exception {

        Cliente c = new Cliente("111.111.111-22", "Givaldo Marques", "(79)981583468", "junioraqw@gmail.com", 0, true);

        when(mockedCliente.buscarCliente(cliente.getCpf())).thenReturn(c);
        String valorPago = String.valueOf(venda.getValorVenda() + 1);//campo de texto
        assertNull(criarVenda.salvarVenda(mockedCliente, valorPago, venda, c.getCpf()));

    }


    /**
     * Salvar vendas com valor pago não-número
     */
    @Test
    public void salvarVendaValorPagoNaoNumero() throws Exception {
        String valorPago = "10A";//campo de texto
        when(mockedCliente.buscarCliente(cliente.getCpf())).thenReturn(cliente);
        assertNull(criarVenda.salvarVenda(mockedCliente, valorPago, venda, cliente.getCpf()));
    }

    /**
     * Salvar vendas com valor pago menor que o valor da venda
     */

    @Test
    public void salvarVendaValorPagoMenor() throws Exception {
        String valorPago = String.valueOf(venda.getValorVenda() - 1);//campo de texto
        when(mockedCliente.buscarCliente(cliente.getCpf())).thenReturn(cliente);
        assertNull(criarVenda.salvarVenda(mockedCliente, valorPago, venda, cliente.getCpf()));
    }


    /**
     * Salvar vendas com cpf do cliente mal formatado
     */

    @Test
    public void salvarVendaCpfMalFormatado() throws Exception {
        Cliente c = new Cliente("111.111.111.22", "Givaldo Marques", "(79)981583468", "junioraqw@gmail.com", 0, true);
        when(mockedCliente.buscarCliente(cliente.getCpf())).thenReturn(c);

        String valorPago = String.valueOf(venda.getValorVenda() + 1);//campo de texto
        assertNull(criarVenda.salvarVenda(mockedCliente, valorPago, venda, c.getCpf()));
    }


    /**
     * Salvar vendas com cliente não cadastrado
     */

    @Test
    public void salvarVendaClienteNaoCadastrado() throws Exception {
        Cliente c = new Cliente("121.111.111-22", "Givaldo Marques", "(79)981583468", "junioraqw@gmail.com", 0, true);
        when(mockedCliente.buscarCliente(cliente.getCpf())).thenReturn(c);

        String valorPago = String.valueOf(venda.getValorVenda() + 1);//campo de texto
        assertNull(criarVenda.salvarVenda(mockedCliente, valorPago, venda, c.getCpf()));
    }


}