package br.com.afazenda.controller;

import br.com.afazenda.model.vo.Produto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ListarProdutosTest {

    private ListarProdutosController produtosController = new ListarProdutosController();
    private Produto produto = new Produto(2, 10, "kg", 100, "carne", 'a', "24/08/2017");

    /**
     * Adiciona 0 itens do produto ao carrinho
     */
    @org.junit.Test
    public void adicionar0Itens() {
        String q = "0";
        boolean result = produtosController.adicionarItemCarrinho(produto, q);
        assertFalse(result);
    }

    /**
     * Adiciona todos os itens no carrinho
     */
    @org.junit.Test
    public void adicionarTodosItens() {
        String q = produto.getQuantidade() + "";
        boolean result = produtosController.adicionarItemCarrinho(produto, q);
        assertTrue(result);
    }

    /**
     * Adiciona mais itens que a quantidade em estoque
     */
    @org.junit.Test
    public void adicionarMaisItens() {
        String q = (produto.getQuantidade() + 1) + "";
        boolean result = produtosController.adicionarItemCarrinho(produto, q);
        assertFalse(result);
    }

    /**
     * Adiciona quantidade negativa de itens
     */
    @org.junit.Test
    public void adicionarNegativaItens() {
        String q = "-1";
        boolean result = produtosController.adicionarItemCarrinho(produto, q);
        assertFalse(result);
    }

    /**
     * Adiciona mais itens de produto inválido
     */
    @org.junit.Test
    public void adicionarProdutoInvalido() {
        String q = (produto.getQuantidade() + 1) + "";
        boolean result = produtosController.adicionarItemCarrinho(null, q);
        assertFalse(result);
    }

    /**
     * Adiciona um valor não-número de itens
     */
    @org.junit.Test
    public void adicionarNaoNumeroProduto() {
        String q = "$a";
        boolean result = produtosController.adicionarItemCarrinho(produto, q);
        assertFalse(result);
    }

    /**
     * Valores válidos
     */
    @org.junit.Test
    public void adicionarValoresValidosProduto() {
        String q = produto.getQuantidade() - 1 + "";
        boolean result = produtosController.adicionarItemCarrinho(produto, q);
        assertTrue(result);
    }

}