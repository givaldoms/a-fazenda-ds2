package utils;

import java.util.regex.Pattern;

/**
 * Created by Givaldo Marques
 * on 28/04/17.
 */
public class RegexMatches {

    public static boolean isCPF(String cpf) {

        //123.345.234-22
        String regex = "(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)";
        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(cpf).find();
    }

    public static boolean isEmail(String email) {

        //user@dominio.com
        String regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\" +
                "x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x" +
                "0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*" +
                "[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0" +
                "-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\" +
                "x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])";

        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(email).find();

    }

    public static boolean isCEP(String cep) {

        //23321-342
        String regex = "^\\d{5}[-]\\d{3}$";
        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(cep).find();
    }

    public static boolean isDataNascimento(String dataNasc) {

        //20/02/3021
        String regex = "(^(((0[1-9]|1[0-9]|2[0-8])[\\/](0[1-9]|1[012]))|((29|30|31)[\\/]" +
                "(0[13578]|1[02]))|((29|30)[\\/](0[4,6,9]|11)))[\\/](19|[2-9][0-9])\\d\\d$)|" +
                "(^29[\\/]02[\\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60" +
                "|64|68|72|76|80|84|88|92|96)$)\n";

        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(dataNasc).find();

    }

    public static boolean isNumber(String number) {

        String regex = "^(0|[1-9][0-9]*)$";
        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(number).find();
    }

    public static boolean isDecimalNumber(String number) {


        //2323.33 (no máximo 2 casas decimais)
        String regex = "^[0-9]+(\\.[0-9]{1,2})?$";
        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(number).find();
    }

    public static boolean isNome(String number) {

        String regex = "[a-z]{2,}";
        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(number).find();
    }

    public static boolean isData(String data) {
        String regex = "^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/([0-9][0-9])?[0-9][0-9]$";
        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(data).find();
    }

}
