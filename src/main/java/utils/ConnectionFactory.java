package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Givaldo Marques
 * on 25/04/17.
 */
public class ConnectionFactory {

    public static Connection getConnection() {
        try {
            String host = "localhost";
            String db = "postgres";
            String url = "jdbc:postgresql://" + host + "/" + db;
            String user = "postgres";
            String senha = "88340120";
            return DriverManager.getConnection(url, user, senha);
        } catch (SQLException e) {
            return null;
        }
    }

}
