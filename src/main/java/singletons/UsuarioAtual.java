package singletons;

import br.com.afazenda.model.vo.Funcionario;

/**
 * Created by Givaldo Marques
 * on 29/04/17.
 * <p>
 * Guarda a instância com os dados do funcionario que está logado.
 */
public class UsuarioAtual {
    public static UsuarioAtual ourInstance = new UsuarioAtual();

    //funcionario logado no sistema
    private Funcionario funcionarioAtual;

    public UsuarioAtual() {
        funcionarioAtual = new Funcionario();
    }

    public static UsuarioAtual getInstance() {
        return ourInstance;
    }

    public Funcionario getFuncionarioAtual() {
        return funcionarioAtual;
    }

    public void setFuncionarioAtual(Funcionario funcionarioAtual) {

        this.funcionarioAtual = funcionarioAtual;
    }

    public void logoffFuncionario() {
        this.funcionarioAtual = null;
    }
}
