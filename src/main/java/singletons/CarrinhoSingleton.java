package singletons;

import br.com.afazenda.model.vo.Venda;

/**
 * Created by Givaldo Marques
 * on 04/05/17.
 */
public class CarrinhoSingleton {
    public static CarrinhoSingleton ourInstance = new CarrinhoSingleton();

    private Venda venda;

    public void logoffFuncionario() {
        this.venda = null;
    }

    public Venda getVenda() {
        if (venda == null) {
            venda = new Venda();
        }
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }
}
