package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import singletons.UsuarioAtual;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by patrick on 02/05/17.
 */
public class MenuPrincipal {

    private static JFrame frame;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JButton agricolaButton;
    private JButton estoqueButton;
    private JButton vendasButton;
    private JButton animalButton;
    private JButton clientesButton;
    private JButton funcionariosButton;
    private JPanel MainPanel;

    public MenuPrincipal() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        funcionariosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuFuncionarios().iniciarJanela();
                frame.dispose();

            }
        });
        clientesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                new MenuCliente().iniciarJanela();
                frame.dispose();
            }
        });
        estoqueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuEstoque().iniciarJanela();
                frame.dispose();
            }
        });
        vendasButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuVendas().iniciarJanela();
                frame.dispose();
            }
        });
        agricolaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuAgricola().iniciarJanela();
                frame.dispose();
            }
        });
        animalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuAnimais().iniciarJanela();
                frame.dispose();
            }
        });
        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                frame.dispose();
            }
        });

    }

    public void iniciarJanela() {
        frame = new JFrame("Menu Principal");
        frame.setContentPane(new MenuPrincipal().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}

