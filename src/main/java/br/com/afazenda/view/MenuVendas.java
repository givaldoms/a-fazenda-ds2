package br.com.afazenda.view;

import br.com.afazenda.controller.CriarVendaController;
import br.com.afazenda.controller.GerarRelatorioVendasController;
import br.com.afazenda.controller.LoginController;
import singletons.UsuarioAtual;

import javax.swing.*;
import java.sql.SQLException;

/**
 * Created by Marina on 05/05/2017.
 */
public class MenuVendas {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JButton verRelatórioButton;
    private JButton novaVendaButton;
    private JButton voltarButton;


    public MenuVendas() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        novaVendaButton.addActionListener(actionEvent -> {
            try {
                new CriarVendaController().iniciarJanela();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            frame.dispose();
        });
        verRelatórioButton.addActionListener(actionEvent -> {
            new GerarRelatorioVendasController().iniciarJanela();
            frame.dispose();
        });
        voltarButton.addActionListener(actionEvent -> {
            new MenuPrincipal().iniciarJanela();
            frame.dispose();
        });
        sairButton.addActionListener(e -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });

    }

    public void iniciarJanela() {
        frame = new JFrame("Vendas");
        frame.setContentPane(new MenuVendas().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
