package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import singletons.UsuarioAtual;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by patrick on 04/05/17.
 */
public class MenuEstoque {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JButton gerenciarProdutosButton;
    private JButton cadastrarProdutoButton;
    private JButton voltarButton;

    public MenuEstoque() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        cadastrarProdutoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new CadastrarProdutos().iniciarJanela();
                frame.dispose();
            }
        });
        gerenciarProdutosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new GerenciarProdutos().iniciarJanela();
                frame.dispose();
            }
        });
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuPrincipal().iniciarJanela();
                frame.dispose();
            }
        });
        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });

    }

    public void iniciarJanela() {
        frame = new JFrame("Estoque");
        frame.setContentPane(new MenuEstoque().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
