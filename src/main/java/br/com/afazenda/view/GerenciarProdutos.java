package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import br.com.afazenda.model.dao.ProdutoImp;
import br.com.afazenda.model.vo.Produto;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on 04/05/17.
 */
public class GerenciarProdutos {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JTextField chaveDeBuscaTextField;
    private JButton buscarButton;
    private JList list1;
    private JButton voltarButton;
    private JButton removerButton;
    private JButton adicionarButton;
    private JTextField quantidadeTextField;
    private JLabel outputLabel;
    private ProdutoImp produtoImp = new ProdutoImp(ConnectionFactory.getConnection());
    private ArrayList<Produto> produto = new ArrayList<Produto>();


    public GerenciarProdutos() {
        chaveDeBuscaTextField.setText("");
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuEstoque().iniciarJanela();
                frame.dispose();
            }
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                adicionarItem();
                initLista();
            }
        });


        removerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                removerItem();
                initLista();
            }
        });
        initLista();
        buscarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                initLista();
            }
        });
    }

    public void iniciarJanela() {
        frame = new JFrame("Estoque");
        frame.setContentPane(new GerenciarProdutos().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void adicionarItem() {
        if (!validaCampos()) return;

        Produto p = produto.get(list1.getSelectedIndex());
        outputLabel.setText("Itens Adicionados com sucesso!");
        try {
            produtoImp.adionarItem(p, Integer.parseInt(quantidadeTextField.getText()));

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void removerItem() {
        if (!validaCampos()) return;

        Produto p = produto.get(list1.getSelectedIndex());
        outputLabel.setText("Itens removidos com sucesso!");
        try {
            produtoImp.removerItem(p, Integer.parseInt(quantidadeTextField.getText()));

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void initLista() {
        try {
            produto = produtoImp.buscarProdutoPorNome(chaveDeBuscaTextField.getText());
            DefaultListModel listModel = new DefaultListModel();
            // listModel.addElement(" Nome da Cultura | Periodo Plantio | Periodo Colheita "  );
            for (Produto percorrer : produto) {
                String origem = (percorrer.getOrigem() == 'v' ? "Vegetal" : "Animal");
                listModel.addElement(" Produto :" + percorrer.getNome() + " | Quantidade : " + percorrer.getQuantidade() + " " + percorrer.getMedida() + " | Preço : R$" + percorrer.getPreco() + " | Data de Recebimento : " + percorrer.getData() + " | Origem : " + origem);
            }
            list1.setModel(listModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean validaCampos() {
        boolean r = true;
        if (quantidadeTextField.getText().trim().isEmpty()) {
            outputLabel.setText("Campo Quantidade Obrigatório");
            r = false;
        } else if (!RegexMatches.isNumber(quantidadeTextField.getText())) {
            outputLabel.setText("Quantidade inválida! Tente apenas números.");
            r = false;
        }
        return r;
    }
}
