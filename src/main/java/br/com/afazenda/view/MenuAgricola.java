package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import singletons.UsuarioAtual;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by patrick on 03/05/17.
 */
public class MenuAgricola {
    private static JFrame frame;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JButton gerenciarPlantioDeCulturaButton;
    private JButton gerenciarCulturaAgricolaButton;
    private JButton voltarButton;
    private JPanel MainPanel;

    public MenuAgricola() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuPrincipal().iniciarJanela();
                frame.dispose();
            }
        });
        gerenciarCulturaAgricolaButton.addActionListener(actionEvent -> {
            new GerenciarCulturaAgricola().iniciarJanela();
            frame.dispose();
        });
        gerenciarPlantioDeCulturaButton.addActionListener(actionEvent -> {
            new GerenciarPlantioDeCultura().iniciarJanela();
            frame.dispose();
        });

        sairButton.addActionListener(e -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });
    }

    public void iniciarJanela() {
        frame = new JFrame("Agrícola");
        frame.setContentPane(new MenuAgricola().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
