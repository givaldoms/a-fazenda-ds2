package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import br.com.afazenda.model.dao.AgriculturaImp;
import br.com.afazenda.model.vo.CulturaAgricola;
import br.com.afazenda.model.vo.Plantacao;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on 03/05/17.
 */
public class GerenciarPlantioDeCultura {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JComboBox culturaAgricolaComboBox;
    private JButton cadastrarButton;
    private JTextField dataDePlantioTextField;
    private JButton voltarButton;
    private JButton removerButton;
    private JList list1;
    private JLabel outputLabel;
    private AgriculturaImp agriculturaImp = new AgriculturaImp(ConnectionFactory.getConnection());
    private ArrayList<CulturaAgricola> culturaAgricola = new ArrayList<CulturaAgricola>();
    private ArrayList<Plantacao> plantioDeCultura = new ArrayList<Plantacao>();


    public GerenciarPlantioDeCultura() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuAgricola().iniciarJanela();
                frame.dispose();
            }
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });

        cadastrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                cadastrarPlantioDeCultura();
                initLista();
            }
        });
        removerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                removerPlantioDeCultura();
                initLista();
            }
        });

        preencherCombo();
        initLista();
    }

    private void initLista() {
        try {
            plantioDeCultura = agriculturaImp.listarPlantioDeCultura();
            DefaultListModel listModel = new DefaultListModel();
            // listModel.addElement(" Nome da Cultura | Periodo Plantio | Periodo Colheita "  );
            for (Plantacao percorrer : plantioDeCultura) {
                listModel.addElement(" Identificação do Plantio:" + percorrer.getIdPlantacao() + " | Cultura : " + percorrer.getCultura().getNome() + " | Data da Plantação: " + percorrer.getDataPlantacao());
            }
            list1.setModel(listModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void preencherCombo() {
        try {
            culturaAgricola = agriculturaImp.listarCulturaAgricola();
            DefaultComboBoxModel listModel = new DefaultComboBoxModel();
            // listModel.addElement(" Nome da Cultura | Periodo Plantio | Periodo Colheita "  );
            for (CulturaAgricola percorrer : culturaAgricola) {
                listModel.addElement(percorrer.getNome());
            }
            culturaAgricolaComboBox.setModel(listModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void cadastrarPlantioDeCultura() {
        if (!validaCampos()) return;

        Plantacao plantacao = new Plantacao();
        dataDePlantioTextField.getText();

        plantacao.setDataPlantacao(dataDePlantioTextField.getText());
        CulturaAgricola c = new CulturaAgricola();
        c = culturaAgricola.get(culturaAgricolaComboBox.getSelectedIndex());
        plantacao.setCultura(c);
        try {
            agriculturaImp.cadastarPlantio(plantacao);
            outputLabel.setText("Cultura Agrícola cadastrado com sucesso!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void removerPlantioDeCultura() {
        try {
            Plantacao plantacaoAuxiliar = plantioDeCultura.get(list1.getSelectedIndex());
            agriculturaImp.excluirPlantioDeCultura(plantacaoAuxiliar);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private boolean validaCampos() {
        boolean r = true;
        if (dataDePlantioTextField.getText().trim().isEmpty()) {
            outputLabel.setText("Todos os campos são obrigatórios");
            r = false;
        } else if (!RegexMatches.isData(dataDePlantioTextField.getText())) {
            outputLabel.setText(" Data inválida. Tente assim : dd-mm-aaaa");
            r = false;
        }

        return r;
    }

    public void iniciarJanela() {
        frame = new JFrame("Menu Principal");
        frame.setContentPane(new GerenciarPlantioDeCultura().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
