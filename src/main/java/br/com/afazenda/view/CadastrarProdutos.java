package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import br.com.afazenda.model.dao.ProdutoImp;
import br.com.afazenda.model.vo.Produto;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on 04/05/17.
 */
public class CadastrarProdutos {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JTextField nomeDoProdutoTextField;
    private JButton adicionarButton;
    private JList list1;
    private JButton voltarButton;
    private JButton apagarButton;
    private JComboBox origemDoProdutoComboBox;
    private JTextField precoTextField;
    private JTextField unidadeDeMedidaTextField;
    private JTextField dataRecebimentoTextField;
    private JTextField quantidadeTextField;
    private JLabel outputLabel;
    private ProdutoImp produtoImp = new ProdutoImp(ConnectionFactory.getConnection());
    private ArrayList<Produto> produto = new ArrayList<Produto>();

    public CadastrarProdutos() {
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new br.com.afazenda.view.MenuEstoque().iniciarJanela();
                frame.dispose();
            }
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                cadastrarProduto();
                initLista();
            }
        });
        apagarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                removerProduto();
                initLista();

            }
        });

        initLista();
    }

    public static void main(String[] args) {
        new CadastrarProdutos().iniciarJanela();
    }

    public void iniciarJanela() {
        frame = new JFrame("Estoque");
        frame.setContentPane(new CadastrarProdutos().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void removerProduto() {
        try {
            Produto produtoAuxiliar = produto.get(list1.getSelectedIndex());
            produtoImp.excluirProduto(produtoAuxiliar);
            outputLabel.setText("Produto removido com sucesso!");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void initLista() {
        try {
            produto = produtoImp.listarProdutos();
            DefaultListModel listModel = new DefaultListModel();
            // listModel.addElement(" Nome da Cultura | Periodo Plantio | Periodo Colheita "  );
            for (Produto percorrer : produto) {
                String origem = (percorrer.getOrigem() == 'v' ? "Vegetal" : "Animal");
                listModel.addElement(" Produto :" + percorrer.getNome() + " | Quantidade : " + percorrer.getQuantidade() + " " + percorrer.getMedida() + " | Preço : R$" + percorrer.getPreco() + " | Data de Recebimento : " + percorrer.getData() + " | Origem : " + origem);
            }
            list1.setModel(listModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void cadastrarProduto() {
        if (!validaCampos()) return;
        Produto p = new Produto();

        p.setNome(nomeDoProdutoTextField.getText());
        p.setData(dataRecebimentoTextField.getText());
        p.setMedida(unidadeDeMedidaTextField.getText());
        String origemDoProdutoChar = ((origemDoProdutoComboBox.getSelectedIndex() == 0) ? "a" : "v");
        p.setOrigem(origemDoProdutoChar.charAt(0));
        String s = precoTextField.getText();
        p.setPreco(Float.parseFloat(s));
        p.setQuantidade(Integer.parseInt(quantidadeTextField.getText()));

        outputLabel.setText("Produto cadastrado com sucesso!");
        try {
            produtoImp.cadastrarProduto(p);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private boolean validaCampos() {
        boolean r = true;

        if (dataRecebimentoTextField.getText().trim().isEmpty() ||
                nomeDoProdutoTextField.getText().trim().isEmpty() ||
                unidadeDeMedidaTextField.getText().trim().isEmpty() ||
                precoTextField.getText().trim().isEmpty() ||
                quantidadeTextField.getText().trim().isEmpty()) {
            outputLabel.setText(" Todos os campos são obrigatórios");
            r = false;
        } else if (!RegexMatches.isNome(nomeDoProdutoTextField.getText())) {
            outputLabel.setText(" Nome inválido! Utilize somente Letras");
            r = false;
        } else if (!RegexMatches.isData(dataRecebimentoTextField.getText())) {
            outputLabel.setText(" Data de Recebimento inválida. Tente assim : dd/mm/aaaa");
            r = false;
        } else if (!RegexMatches.isDecimalNumber(precoTextField.getText())) {
            outputLabel.setText(" Preço inválido. Tente utilizar : 00.00");
            r = false;
        } else if (!RegexMatches.isNumber(quantidadeTextField.getText())) {
            outputLabel.setText(" Quantidade inválida. Tente apenas números");
            r = false;
        }

        return r;
    }

}
