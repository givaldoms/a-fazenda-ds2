package br.com.afazenda.view;

import singletons.UsuarioAtual;

import javax.swing.*;

/**
 * Created by patrick on 04/05/17.
 */
public class ConfiguracoesDeUsuario {

    private static JFrame frame;
    private JLabel nomeUsuarioAtualLabel;
    private JPanel MainPanel;

    public ConfiguracoesDeUsuario() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
    }


    public void iniciarJanela() {
        frame = new JFrame("Configurações");
        frame.setContentPane(new ConfiguracoesDeUsuario().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
