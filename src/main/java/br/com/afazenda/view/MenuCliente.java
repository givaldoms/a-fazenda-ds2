package br.com.afazenda.view;

import br.com.afazenda.controller.CadastroClienteController;
import br.com.afazenda.controller.LoginController;
import br.com.afazenda.controller.PesquisarClientesController;
import singletons.UsuarioAtual;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by patrick on 02/05/17.
 */
public class MenuCliente {
    private static JFrame frame;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JButton gerenciarClientesButton;
    private JButton cadastrarClienteButton;
    private JButton voltarButton;
    private JPanel MainPanel;

    public MenuCliente() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuPrincipal().iniciarJanela();
                frame.dispose();
            }
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
        cadastrarClienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CadastroClienteController().iniciarJanela();
                frame.dispose();
            }
        });
        gerenciarClientesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new PesquisarClientesController().iniciarJanela();
                frame.dispose();
            }
        });
    }

    public void iniciarJanela() {
        frame = new JFrame("Clientes");
        frame.setContentPane(new MenuCliente().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }
}
