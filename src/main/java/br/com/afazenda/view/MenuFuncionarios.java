package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import br.com.afazenda.controller.PesquisarCargosController;
import br.com.afazenda.controller.PesquisarFuncionariosController;
import singletons.UsuarioAtual;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by patrick on 02/05/17.
 */
public class MenuFuncionarios {
    private static JFrame frame;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JButton gerenciarCargosButton;
    private JButton gerenciarFuncionariosButton;
    private JButton gerenciarContratosButton;
    private JButton voltarButton;
    private JPanel MainPanel;

    public MenuFuncionarios() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuPrincipal().iniciarJanela();
                frame.dispose();
            }
        });
        gerenciarCargosButton.addActionListener(new ActionListener() {
            @Override
            /**
             Não funciona
             */
            public void actionPerformed(ActionEvent actionEvent) {
                new PesquisarCargosController().iniciarJanela();
                frame.dispose();
            }
        });
        gerenciarFuncionariosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new PesquisarFuncionariosController().iniciarJanela();
                frame.dispose();

            }
        });
        gerenciarContratosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuContrato().iniciarJanela();
                frame.dispose();
            }
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
    }

    public void iniciarJanela() {
        frame = new JFrame("Funcionários");
        frame.setContentPane(new MenuFuncionarios().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

}
