package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import br.com.afazenda.model.dao.EspecieImp;
import br.com.afazenda.model.vo.Especie;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on 02/05/17.
 */
public class GerenciarEspecies {

    private static JFrame frame;
    private JPanel MainPanel;
    private JButton configuraçõesButton;
    private JButton sairButton;
    private JList list1;
    private JButton deletarButton1;
    private JButton voltarButton;
    private JButton cadastrarButton;
    private JTextField textField1;
    private JLabel nomeUsuarioAtualLabel;
    private JLabel outputLabel;
    private EspecieImp especieImp = new EspecieImp(ConnectionFactory.getConnection());
    private ArrayList<Especie> especie = new ArrayList<Especie>();


    public GerenciarEspecies() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        outputLabel.setText("");
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuAnimais().iniciarJanela();
                frame.dispose();
            }
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });

        list1.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                preencheLista();
            }
        });
        cadastrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cadastrarEspecie();
                list1.setModel(new DefaultListModel());
                preencheLista();
            }
        });
        deletarButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                removerEspecie();
                preencheLista();
            }
        });
    }

    public void iniciarJanela() {
        frame = new JFrame("Animal");
        frame.setContentPane(new GerenciarEspecies().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    private void preencheLista() {
        try {
            //EspecieImp especieImp = new EspecieImp(ConnectionFactory.getConnection());
            especie = especieImp.getEspecies();
            DefaultListModel modelo = new DefaultListModel();
            int d = especie.size();
            Especie e;
            for (int i = 0; i < d; i++) {
                e = especie.get(i);
                modelo.addElement(e.getName());
            }
            list1.setModel(modelo);
        } catch (SQLException x) {
            x.printStackTrace();
        }
    }

    private boolean validaCampos() {
        boolean r = true;
        if (textField1.getText().trim().isEmpty()) {
            outputLabel.setText("Todos os campos são obrigatórios");
            r = false;
        } else if (!RegexMatches.isNome(textField1.getText())) {
            outputLabel.setText("Nome inválido! Utilize apenas Letras.");
            r = false;
        }

        return r;
    }

    private void cadastrarEspecie() {
        if (!validaCampos()) return;

        try {
            especieImp.insereEspecie(textField1.getText(), 0);

            outputLabel.setText("Espécie cadastrada com sucesso");

        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                outputLabel.setText("Esta espécie já está cadastrada");
            } else {
                outputLabel.setText("Houve um erro ao realizar conexão com o banco de dados");
            }

        }
    }

    private void removerEspecie() {

        try {
            especieImp.excluirEspecie(especie.get(list1.getSelectedIndex()));
            outputLabel.setText("Espécie removida com sucesso");

        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                outputLabel.setText("Esta espécie já foi removida");
            } else {
                outputLabel.setText("Houve um erro ao realizar conexão com o banco de dados");
            }

        }
    }
}