package br.com.afazenda.view;

import br.com.afazenda.controller.CadastroContratoController;
import br.com.afazenda.controller.LoginController;
import singletons.UsuarioAtual;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by patrick on 03/05/17.
 */
public class MenuContrato {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JButton pesquisarContratoButton;
    private JButton adicionarContratoButton;
    private JButton voltarButton;

    public MenuContrato() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuFuncionarios().iniciarJanela();
                frame.dispose();
            }
        });
        adicionarContratoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new CadastroContratoController().iniciarJanela();
                frame.dispose();
            }
        });
        pesquisarContratoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new PesquisarContrato().iniciarJanela();
                frame.dispose();
            }
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
    }

    public void iniciarJanela() {
        frame = new JFrame("Funcionários");
        frame.setContentPane(new MenuContrato().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
