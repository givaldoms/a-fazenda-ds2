package br.com.afazenda.view;

import br.com.afazenda.controller.LoginController;
import br.com.afazenda.model.dao.AgriculturaImp;
import br.com.afazenda.model.vo.CulturaAgricola;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on 03/05/17.
 */
public class GerenciarCulturaAgricola {
    private static JFrame frame;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JPanel MainPanel;
    private JTextField nomeTextField;
    private JButton cadastrarButton;
    private JButton voltarButton;
    private JButton removerButton;
    private JTextField DataDePlantioTextField;
    private JTextField DataDeColheitaTextField;
    private JList list1;
    private JLabel outputLabel;
    private AgriculturaImp agriculturaImp = new AgriculturaImp(ConnectionFactory.getConnection());
    private ArrayList<CulturaAgricola> culturaAgricola = new ArrayList<CulturaAgricola>();

    public GerenciarCulturaAgricola() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        voltarButton.addActionListener(actionEvent -> {
            new MenuAgricola().iniciarJanela();
            frame.dispose();
        });
        sairButton.addActionListener(e -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });

        removerButton.addActionListener(actionEvent -> {
            removerCulturaAgricola();
            initLista();
            outputLabel.setText("");
        });
        cadastrarButton.addActionListener(actionEvent -> {
            cadastrarCulturaAgricola();
            initLista();

        });

        initLista();
    }

    private void removerCulturaAgricola() {
        try {
            CulturaAgricola culturaAgricolaAuxiliar = culturaAgricola.get(list1.getSelectedIndex());
            agriculturaImp.excluirCulturaAgricola(culturaAgricolaAuxiliar);
            outputLabel.setText("Cultura Agrícola removida com sucesso!");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void initLista() {
        try {
            culturaAgricola = agriculturaImp.listarCulturaAgricola();
            DefaultListModel listModel = new DefaultListModel();
            // listModel.addElement(" Nome da Cultura | Periodo Plantio | Periodo Colheita "  );
            for (CulturaAgricola percorrer : culturaAgricola) {
                listModel.addElement(" Nome da Cultura:" + percorrer.getNome() + " | Periodo Plantio : " + percorrer.getPeriodoPlantio() + " | Periodo Colheita: " + percorrer.getPeriodoColheita());
            }
            list1.setModel(listModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void iniciarJanela() {
        frame = new JFrame("Agrícola");
        frame.setContentPane(new GerenciarCulturaAgricola().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    private void cadastrarCulturaAgricola() {
        if (!validaCampos()) return;

        CulturaAgricola c = new CulturaAgricola();
        nomeTextField.getText();

        c.setNome(nomeTextField.getText());
        c.setPeriodoColheita(DataDeColheitaTextField.getText());
        c.setPeriodoPlantio(DataDePlantioTextField.getText());
        outputLabel.setText("Cultura Agrícola cadastrado com sucesso!");
        try {
            agriculturaImp.cadastrarCulturaAgricola(c);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    private boolean validaCampos() {
        boolean r = true;
        if (DataDeColheitaTextField.getText().trim().isEmpty() ||
                DataDePlantioTextField.getText().trim().isEmpty() ||
                nomeTextField.getText().trim().isEmpty()) {
            outputLabel.setText("Todos os campos são obrigatórios");
            r = false;
        } else if (!RegexMatches.isNome(nomeTextField.getText())) {
            outputLabel.setText(" Nome inválido. Tente somente Letras.");
            r = false;
        }

        return r;
    }


}
