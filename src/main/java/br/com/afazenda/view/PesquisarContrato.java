package br.com.afazenda.view;

import br.com.afazenda.controller.CadastroContratoController;
import br.com.afazenda.controller.LoginController;
import br.com.afazenda.model.dao.ContratoImp;
import br.com.afazenda.model.vo.Contrato;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on 03/05/17.
 */
@SuppressWarnings("Duplicates")
public class PesquisarContrato {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JTextField CPFtextField;
    private JButton buscarButton;
    private JList list1;
    private JTextArea textArea1;
    private JButton voltarButton;
    private JButton inativarButton;
    private JButton novoButton;
    private JButton listarTodosButton;
    private ArrayList<Contrato> contratos;
    private ContratoImp contratoImp = new ContratoImp(ConnectionFactory.getConnection());


    public PesquisarContrato() {

        try {
            contratos = contratoImp.listarContratos();
            preencherListaTodos();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new MenuContrato().iniciarJanela();
                frame.dispose();
            }
        });
        novoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new CadastroContratoController().iniciarJanela();
                frame.dispose();
            }
        });


        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });

        listarTodosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                preencherListaTodos();

            }
        });

        inativarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = list1.getSelectedIndex();
                try {
                    contratoImp.inativarContrato(contratos.get(index));
                    preencherListaTodos();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
        });
        buscarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                preencherListaPorCPF();
            }
        });
    }

    public static void main(String[] args) {
        new PesquisarContrato().iniciarJanela();
    }

    public void iniciarJanela() {
        frame = new JFrame("Funcionários");
        frame.setContentPane(new PesquisarContrato().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    private void preencherListaTodos() {
        try {
            contratos = contratoImp.listarContratos();
            DefaultListModel listModel = new DefaultListModel();

            for (Contrato contrato : contratos) {
                String estado;
                estado = contrato.isAtivo() ? "ativo" : "inativo";

                String s = "Id do contrato: " + contrato.getId() + " | CPF: " + contrato.getFuncionario().getCpf() + "" +
                        " | Cargo: " + contrato.getCargo().getNome() + " | Status: " + estado;
                listModel.addElement(s);
            }
            list1.setModel(listModel);


        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    private void preencherListaPorCPF() {
        try {
            String CPF = CPFtextField.getText();
            contratos = contratoImp.listarContratosPorCPF(CPF);
            DefaultListModel listModel = new DefaultListModel();

            for (Contrato c : contratos) {
                String estado;
                estado = c.isAtivo() ? "ativo" : "inativo";

                String s = "Id do contrato: " + c.getId() + " | CPF: " + c.getFuncionario().getCpf() + "" +
                        " | Cargo: " + c.getCargo().getNome() + " | Status: " + estado;
                listModel.addElement(s);
            }
            list1.setModel(listModel);


        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

}
