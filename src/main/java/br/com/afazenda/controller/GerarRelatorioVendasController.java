package br.com.afazenda.controller;

import br.com.afazenda.model.dao.VendaImp;
import br.com.afazenda.model.vo.Produto;
import br.com.afazenda.view.MenuVendas;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Marina && Givaldo Marques
 * on 04/05/2017.
 */
public class GerarRelatorioVendasController {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuraçõesButton;
    private JButton sairButton;
    private JList<String> list1;
    private JButton voltarButton;
    private JLabel outputLabel;

    public GerarRelatorioVendasController() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        outputLabel.setText("");
        voltarButton.addActionListener(actionEvent -> {
            new MenuVendas().iniciarJanela();
            frame.dispose();
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });

        list1.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                preencheLista();
            }
        });

    }

    public void iniciarJanela() {
        frame = new JFrame("Vendas");
        frame.setContentPane(new GerarRelatorioVendasController().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    private void preencheLista() {
        try {
            VendaImp vendaImp = new VendaImp(ConnectionFactory.getConnection());
            ArrayList<Produto> produtos = vendaImp.listarProdutosVendidos();

            DefaultListModel<String> modelo = new DefaultListModel<>();
            int d = produtos.size();
            Produto produto;
            for (Produto p : produtos) {
                produto = p;
                modelo.addElement(produto.getNome());
            }
            list1.setModel(modelo);
        } catch (SQLException x) {
            x.printStackTrace();
        }
    }

}
