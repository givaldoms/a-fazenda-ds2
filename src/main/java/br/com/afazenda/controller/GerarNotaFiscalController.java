package br.com.afazenda.controller;

import br.com.afazenda.model.dao.VendaImp;
import br.com.afazenda.model.vo.ItemProduto;
import singletons.CarrinhoSingleton;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;

import javax.swing.*;
import java.sql.SQLException;

/**
 * Created by patrick on 03/05/17.
 */
public class GerarNotaFiscalController {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton sairButton;
    private JList<String> listaProdutosComprados;
    private JButton voltarButton;
    private JButton confirmarButton;
    private JLabel valorTotalLabel;
    private JLabel valorPagoLabel;
    private JLabel trocoLabel;
    private JLabel vendedorLabel;
    private JLabel cpfClienteLabel;

    public GerarNotaFiscalController() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());

        vendedorLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        CarrinhoSingleton.ourInstance.getVenda().setFuncionario(UsuarioAtual.getInstance().getFuncionarioAtual());
        cpfClienteLabel.setText(CarrinhoSingleton.ourInstance.getVenda().getCliente().getCpf());

        valorPagoLabel.setText(CarrinhoSingleton.ourInstance.getVenda().getValorPago() + "");
        valorTotalLabel.setText(CarrinhoSingleton.ourInstance.getVenda().getValorVenda() + "");
        trocoLabel.setText(CarrinhoSingleton.ourInstance.getVenda().getTroco() + "");

        preencheLista();

        voltarButton.addActionListener(e -> {
            try {
                new CriarVendaController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });

        confirmarButton.addActionListener(e -> {
            VendaImp vendaImp = new VendaImp(ConnectionFactory.getConnection());
            if (vendaImp.insereVenda(CarrinhoSingleton.ourInstance.getVenda())) {
                JOptionPane.showMessageDialog(null, "Venda realizada com sucesso", "", JOptionPane.INFORMATION_MESSAGE);
                frame.dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Error ao realizar venda", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private void preencheLista() {
        float precoTotal = 0;

        DefaultListModel<String> modelo = new DefaultListModel<>();
        for (ItemProduto p : CarrinhoSingleton.ourInstance.getVenda().getItens()) {
            float precoItem = p.getProduto().getPreco() * p.getQuantidade();

            //Arroz 10 x 3 = 30
            String s = p.getProduto().getNome() + " - " + p.getProduto().getPreco() + " x " + p
                    .getQuantidade() + " = " + precoItem;
            modelo.addElement(s);

            precoTotal += precoItem;
        }

        listaProdutosComprados.setModel(modelo);

    }

    public void iniciarJanela() {
        frame = new JFrame("Venda");
        frame.setContentPane(new GerarNotaFiscalController().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
