package br.com.afazenda.controller;

import br.com.afazenda.model.dao.ClienteImp;
import br.com.afazenda.model.dao.EnderecoImp;
import br.com.afazenda.model.vo.Cliente;
import br.com.afazenda.model.vo.Endereco;
import br.com.afazenda.view.ConfiguracoesDeUsuario;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by gallotropo on 5/3/17.
 */
@SuppressWarnings("Duplicates")
public class EditClienteController implements ActionListener {

    private static JFrame frame;
    private JButton configuraçõesButton;
    private JButton sairButton;
    private JTextField NomeTextField;
    private JComboBox sexoComboBox;
    private JTextField emailTextField;
    private JTextField cpfTextField;
    private JTextField cepTextField;
    private JTextField logradouroTextField;
    private JTextField numeroTextField;
    private JTextField cidadeTextField;
    private JTextField bairroTextField;
    private JComboBox estadoComboBox;
    private JButton salvarButton;
    private JButton cancelarButton;
    private JTextField telefoneTextField;
    private JPanel MainPainel;
    private JLabel outputLabel;
    private String cpf;
    private Endereco endereco;
    private JLabel nomeUsuarioAtualLabel;

    public EditClienteController() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        configuraçõesButton.addActionListener(this);
        sairButton.addActionListener(this);
        salvarButton.addActionListener(this);
        cancelarButton.addActionListener(this);

        configuraçõesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ConfiguracoesDeUsuario().iniciarJanela();
                frame.dispose();
            }
        });

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
    }

    public EditClienteController(String cpf) throws SQLException {
        configuraçõesButton.addActionListener(this);
        sairButton.addActionListener(this);
        salvarButton.addActionListener(this);
        cancelarButton.addActionListener(this);
        ClienteImp clienteImp = new ClienteImp(ConnectionFactory.getConnection());
        Cliente cliente = clienteImp.buscarCliente(cpf);
        EnderecoImp enderecoImp = new EnderecoImp(ConnectionFactory.getConnection());
        endereco = enderecoImp.getEndereco(cliente.getEndereco());
        cpfTextField.setText(cpf);
        System.out.println(cliente.getNome());

        NomeTextField.setText(cliente.getNome());
        emailTextField.setText(cliente.getEmail());
        telefoneTextField.setText(cliente.getTelefone());
        cepTextField.setText(endereco.getCep());
        logradouroTextField.setText(endereco.getLogradouro());
        numeroTextField.setText(Integer.toString(endereco.getNumero()));
        cidadeTextField.setText(endereco.getCidade());
        bairroTextField.setText(endereco.getBairro());

    }

    public void iniciarJanela() {
        JFrame frame = new JFrame("Editar Cliente");
        frame.setContentPane(new EditClienteController().MainPainel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    public void iniciarJanela(String cpf) throws SQLException {
        JFrame frame = new JFrame("Editar Cliente");
        frame.setContentPane(new EditClienteController(cpf).MainPainel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    private boolean validarCampos() {

        outputLabel.setText("<html>");

        if (NomeTextField.getText().trim().isEmpty() ||
                emailTextField.getText().trim().isEmpty() ||
                cpfTextField.getText().trim().isEmpty() ||
                logradouroTextField.getText().trim().isEmpty() ||
                cepTextField.getText().trim().isEmpty() ||
                cidadeTextField.getText().trim().isEmpty() ||
                numeroTextField.getText().trim().isEmpty() ||
                telefoneTextField.getText().trim().isEmpty() ||
                bairroTextField.getText().trim().isEmpty() ||
                sexoComboBox.getSelectedIndex() == 0 ||
                estadoComboBox.getSelectedIndex() == 0

                ) {
            outputLabel.setText("Todos os campos são obrigatórios");
            return false;
        }

        String nome = NomeTextField.getText();
        if (!RegexMatches.isNome(nome)) {
            outputLabel.setText(outputLabel.getText() + "<br>Nome inválido");
            return false;
        }

        String email = emailTextField.getText();
        if (!RegexMatches.isEmail(email)) {
            outputLabel.setText(outputLabel.getText() + "<br>Email inválido. Ex: " +
                    "user@dominio.com");
            return false;
        }

        String cpf = cpfTextField.getText();
        if (!RegexMatches.isCPF(cpf)) {
            outputLabel.setText(outputLabel.getText() + "<br>CPF inválido. Ex: " +
                    "012.331.442-21");
            return false;
        }

        String numeroCasa = numeroTextField.getText();
        if (!RegexMatches.isNumber(numeroCasa)) {
            outputLabel.setText(outputLabel.getText() + "<br>Número da casa inválido");
            return false;
        }
        outputLabel.setText(outputLabel.getText() + "</html>");


        return true;
    }

    private void editarCliente() {

        if (!validarCampos()) return;

        try {
            Endereco endereco = new Endereco(
                    logradouroTextField.getText(),
                    Integer.parseInt(numeroTextField.getText()),
                    bairroTextField.getText(),
                    cidadeTextField.getText(),
                    estadoComboBox.getSelectedItem().toString(),
                    cepTextField.getText());
            EnderecoImp NovoEnderecoImp = new EnderecoImp(ConnectionFactory.getConnection());

            ClienteImp novoClienteIMP = new ClienteImp(ConnectionFactory.getConnection());
            Cliente novoCliente = new Cliente();
            if (!NovoEnderecoImp.equals(this.endereco))
                novoCliente.setEndereco(NovoEnderecoImp.insereEndereco(endereco));
            else novoCliente.setEndereco(0);
            novoCliente.setNome(NomeTextField.getText());
            novoCliente.setEmail(emailTextField.getText());
            novoCliente.setTelefone(telefoneTextField.getText());
            novoCliente.setInadiplente(false);
            novoCliente.setCPF(cpfTextField.getText());


            novoClienteIMP.editarCliente(novoCliente, this.cpf);

            outputLabel.setText("Cliente editado com sucesso");

        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                outputLabel.setText("Este CPF já está cadastrado");
            } else {
                outputLabel.setText("Houve um erro ao realizar conexão com o banco de dados");
            }
            e.printStackTrace();

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == salvarButton) {
            editarCliente();
        }
    }

}
