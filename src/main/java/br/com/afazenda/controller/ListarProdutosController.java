package br.com.afazenda.controller;

import br.com.afazenda.model.dao.ProdutoImp;
import br.com.afazenda.model.vo.ItemProduto;
import br.com.afazenda.model.vo.Produto;
import singletons.CarrinhoSingleton;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on
 * 03/05/17.
 */
public class ListarProdutosController {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JTextField produtoTextField;
    private JButton buscarButton;
    private JList<String> produtosJlist;
    private JTextField quantidadeTextField;
    private JButton voltarButton;
    private JButton adicionarItemButton;
    private JButton listarTodos;
    private JLabel outputLabel;
    private ProdutoImp produtoImp = new ProdutoImp(ConnectionFactory.getConnection());
    private ArrayList<Produto> produtoArrayList;

    public ListarProdutosController() {

        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());

        sairButton.addActionListener(actionEvent -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            frame.dispose();
        });

        listarTodos.addActionListener(e -> {
            try {
                produtoArrayList = produtoImp.listarProdutos();
                preencheLista();

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        });

        buscarButton.addActionListener(e -> {
            outputLabel.setText("");

            String nomeProduto = produtoTextField.getText();

            try {
                produtoArrayList = produtoImp.listarProdutos();
                ArrayList<Produto> aux = new ArrayList<>();

                for (Produto p : produtoArrayList) {
                    if (p.getNome().toLowerCase().contains(nomeProduto.toLowerCase())) {
                        aux.add(p);
                    }
                }

                produtoArrayList = aux;
                preencheLista();

            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        });

        adicionarItemButton.addActionListener(e -> {
            String quantidade = quantidadeTextField.getText();
            Produto p = produtoArrayList.get(produtosJlist.getSelectedIndex());

            adicionarItemCarrinho(p, quantidade);
        });

        voltarButton.addActionListener(e -> {
            try {
                new CriarVendaController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });

        try {
            produtoArrayList = produtoImp.listarProdutos();
            preencheLista();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public boolean adicionarItemCarrinho(Produto p, String quantidade) {
        if (!RegexMatches.isNumber(quantidade)) {
            outputLabel.setText("Quantidade inválida");
            return false;
        }

        if (p == null) {
            outputLabel.setText("Produto inválido");
            return false;
        }

        int q = Integer.parseInt(quantidade);

        if (q <= 0 || q > p.getQuantidade()) {
            outputLabel.setText("Quantidade de itens indisponíveis");
            return false;
        }

        ItemProduto itemProduto = new ItemProduto(q, p);

        CarrinhoSingleton.ourInstance.getVenda().getItens().add(itemProduto);
        outputLabel.setText("Produto adicionado");
        quantidadeTextField.setText("");
        preencheLista();
        return true;
    }

    private void preencheLista() {
        boolean possuiNoCarrinho = false;
        DefaultListModel<String> modelo = new DefaultListModel<>();
        for (Produto p : produtoArrayList) {

            for (ItemProduto i : CarrinhoSingleton.ourInstance.getVenda().getItens()) {
                if (i.getProduto().getId() == p.getId()) {
                    possuiNoCarrinho = true;
                }
            }

            if (!possuiNoCarrinho) {
                modelo.addElement(p.getNome() + ": " + p.getQuantidade() + " itens restantes");
            }

            possuiNoCarrinho = false;
        }

        produtosJlist.setModel(modelo);

    }

    public void iniciarJanela() {
        frame = new JFrame("Venda");
        frame.setContentPane(new ListarProdutosController().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        nomeUsuarioAtualLabel.setText(new UsuarioAtual().getFuncionarioAtual().getName());
    }
}
