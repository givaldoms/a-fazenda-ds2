package br.com.afazenda.controller;

import br.com.afazenda.model.dao.CargoImp;
import br.com.afazenda.model.dao.ContratoImp;
import br.com.afazenda.model.dao.FuncionarioImp;
import br.com.afazenda.model.vo.Cargo;
import br.com.afazenda.model.vo.Contrato;
import br.com.afazenda.model.vo.Funcionario;
import br.com.afazenda.view.MenuContrato;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick && Givaldo Marques
 * on 03/05/17.
 */
public class CadastroContratoController implements ActionListener {
    private static JFrame frame;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JComboBox cargoComboBox;
    private JTextField dataInicioTextField;
    private JTextField DataTerminoTextField;
    private JTextField salarioTextField;
    private JTextField cpfTextField;
    private JButton confirmarButton;
    private JButton voltarButton;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JLabel outputLabel;
    private ArrayList<Cargo> cargosArrayList;

    //funcionario a ser contratado
    private Funcionario funcionario;

    public CadastroContratoController() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        confirmarButton.addActionListener(this);
        voltarButton.addActionListener(this);
        sairButton.addActionListener(this);

        CargoImp cargoImp = new CargoImp(ConnectionFactory.getConnection());
        try {
            cargosArrayList = cargoImp.listarCargos();
            for (Cargo cargo : cargosArrayList) {
                cargoComboBox.addItem(cargo.getNome());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        sairButton.addActionListener(e -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });
    }

    public void iniciarJanela() {
        frame = new JFrame("Funcionários");
        frame.setContentPane(new CadastroContratoController().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private boolean buscarFuncionarioContratado(String cpf) {
        try {
            FuncionarioImp funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
            funcionario = funcionarioImp.buscarFuncionario(cpf);
            if (funcionario == null) {
                outputLabel.setText("Nenhum funcionário localizado, verifique se o CPF digitado " +
                        "pertence a algum funcionario");
                return false;
            }

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    private boolean validarCampos(String cpf, String salario, String dataInicio, String dataFim) {

        boolean r = true;

        outputLabel.setText("<html>");

        if (dataInicio.trim().isEmpty() || dataFim.trim().isEmpty() ||
                salario.trim().isEmpty() ||
                cpf.trim().isEmpty()) {

            outputLabel.setText("Todos os campos são obrigatórios");
            System.out.println("validar campos error 1");
            return false;
        }

        if (!RegexMatches.isData(dataInicio) || !RegexMatches.isData(dataFim)) {
            outputLabel.setText(outputLabel.getText() + "<br>Data inválida. Ex: 12/02/2010");
            System.out.println("validar campos error 2");
            r = false;
        } else if (dataInicio.compareTo(dataFim) != -1) {
            outputLabel.setText(outputLabel.getText() + "<br>Data de início e término inválidas");
            System.out.println("validar campos error 1");
            r = false;

        }

        if (!RegexMatches.isCPF(cpf)) {
            outputLabel.setText(outputLabel.getText() + "<br>CPF inválido. Ex: 011.303.212-33");
            System.out.println("validar campos error 1");
            r = false;
        }

        if (!RegexMatches.isDecimalNumber(salario.toString())) {
            outputLabel.setText(outputLabel.getText() + "<br>Salário inválido. Ex: 998.23 ");
            System.out.println("validar campos error 1");
            r = false;
        }

        outputLabel.setText(outputLabel.getText() + "</html>");

        return r;
    }


    public Contrato criarContrato(String cpf, String salario, String dataInicio, String dataFim, Cargo cargo) {

        if (!validarCampos(cpf, salario, dataInicio, dataFim)) {
            System.out.println("Erro1");
            return null;
        }
        if (!buscarFuncionarioContratado(cpf)) {
            System.out.println("Erro2");
            return null;
        }
        if (cargo == null) {
            System.out.println("Erro3");
            return null;
        }

        Contrato contrato = new Contrato();
        contrato.setAtivo(true);
        contrato.setDataContratacao(dataInicio);
        contrato.setDataTermino(dataFim);
        contrato.setSalario(Float.parseFloat(salario));
        contrato.setFuncionario(funcionario);
        contrato.setCargo(cargo);
        return contrato;


    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == voltarButton) {
            new MenuContrato().iniciarJanela();
            frame.dispose();
        } else if (e.getSource() == confirmarButton) {
            String cpf = cpfTextField.getText();
            String salario = salarioTextField.getText();
            String dataInicio = dataInicioTextField.getText();
            String dataFim = DataTerminoTextField.getText();
            Cargo cargo = cargosArrayList.get(cargoComboBox.getSelectedIndex());

            try {
                ContratoImp contratoImp = new ContratoImp(ConnectionFactory.getConnection());
                contratoImp.definirContrato(criarContrato(cpf, salario, dataInicio, dataFim, cargo));

                outputLabel.setText("Contrato realizado com sucesso");
                System.out.println("EI");

            } catch (SQLException ex) {
                outputLabel.setText("Erro ao realizar contrato");
                System.out.println("CadastrarContrato: " + ex.getMessage());
                System.out.println("EIA");

            }
        }
    }
}
