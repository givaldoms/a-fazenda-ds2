package br.com.afazenda.controller;

import br.com.afazenda.model.dao.ClienteImp;
import br.com.afazenda.model.dao.EnderecoImp;
import br.com.afazenda.model.vo.Cliente;
import br.com.afazenda.model.vo.Endereco;
import br.com.afazenda.view.MenuCliente;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by Natã e Elias Rabelo on 27/04/17.
 */
public class CadastroClienteController implements ActionListener {

    private static JFrame frame;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuraçõesButton;
    private JButton sairButton;
    private JTextField NomeTextField;
    private JComboBox sexoComboBox;
    private JTextField emailTextField;
    private JTextField cpfTextField;
    private JTextField cepTextField;
    private JTextField logradouroTextField;
    private JTextField numeroTextField;
    private JTextField cidadeTextField;
    private JTextField bairroTextField;
    private JComboBox estadoComboBox;
    private JButton confirmarButton;
    private JButton cancelarButton;
    private JTextField telefoneTextField;
    private JPanel MainPainel;
    private JLabel outputLabel;


    public CadastroClienteController() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());

        sairButton.addActionListener(this);
        confirmarButton.addActionListener(this);

        NomeTextField.setText("maria");
        emailTextField.setText("maria@gmail.com");
        cpfTextField.setText("233.322.331-32");
        cepTextField.setText("38829-200");
        logradouroTextField.setText("B");
        numeroTextField.setText("200");
        cidadeTextField.setText("aracaju");
        bairroTextField.setText("centro");
        telefoneTextField.setText("(79)23323322");
        estadoComboBox.setSelectedIndex(1);
        sexoComboBox.setSelectedIndex(1);

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MenuCliente().iniciarJanela();
                frame.dispose();
            }
        });
    }


    public void iniciarJanela() {
        JFrame frame = new JFrame("Cadastrar Cliente");
        frame.setContentPane(new CadastroClienteController().MainPainel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    @SuppressWarnings("Duplicates")
    private boolean validarCampos() {

        outputLabel.setText("<html>");

        if (NomeTextField.getText().trim().isEmpty() ||
                emailTextField.getText().trim().isEmpty() ||
                cpfTextField.getText().trim().isEmpty() ||
                logradouroTextField.getText().trim().isEmpty() ||
                cepTextField.getText().trim().isEmpty() ||
                cidadeTextField.getText().trim().isEmpty() ||
                numeroTextField.getText().trim().isEmpty() ||
                telefoneTextField.getText().trim().isEmpty() ||
                bairroTextField.getText().trim().isEmpty() ||
                sexoComboBox.getSelectedIndex() == 0 ||
                estadoComboBox.getSelectedIndex() == 0

                ) {
            outputLabel.setText("Todos os campos são obrigatórios");
            return false;
        }

        String nome = NomeTextField.getText();
        if (!RegexMatches.isNome(nome)) {
            outputLabel.setText(outputLabel.getText() + "<br>Nome inválido");
            return false;
        }

        String email = emailTextField.getText();
        if (!RegexMatches.isEmail(email)) {
            outputLabel.setText(outputLabel.getText() + "<br>Email inválido. Ex: " +
                    "user@dominio.com");
            return false;
        }

        String cpf = cpfTextField.getText();
        if (!RegexMatches.isCPF(cpf)) {
            outputLabel.setText(outputLabel.getText() + "<br>CPF inválido. Ex: " +
                    "012.331.442-21");
            return false;
        }

        String numeroCasa = numeroTextField.getText();
        if (!RegexMatches.isNumber(numeroCasa)) {
            outputLabel.setText(outputLabel.getText() + "<br>Número da casa inválido");
            return false;
        }
        outputLabel.setText(outputLabel.getText() + "</html>");


        return true;
    }

    private void cadastrarCliente() {

        if (!validarCampos()) return;

        try {
            Endereco endereco = new Endereco(
                    logradouroTextField.getText(),
                    Integer.parseInt(numeroTextField.getText()),
                    bairroTextField.getText(),
                    cidadeTextField.getText(),
                    estadoComboBox.getSelectedItem().toString(),
                    cepTextField.getText());
            EnderecoImp NovoEnderecoImp = new EnderecoImp(ConnectionFactory.getConnection());
            ClienteImp novoClienteIMP = new ClienteImp(ConnectionFactory.getConnection());
            Cliente novoCliente = new Cliente();
            novoCliente.setEndereco(NovoEnderecoImp.insereEndereco(endereco));
            novoCliente.setNome(NomeTextField.getText());
            novoCliente.setEmail(emailTextField.getText());
            novoCliente.setTelefone(telefoneTextField.getText());
            novoCliente.setInadiplente(false);
            novoCliente.setCPF(cpfTextField.getText());

            novoClienteIMP.cadastrarCliente(novoCliente);

            outputLabel.setText("Cliente cadastrado com sucesso");

        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                outputLabel.setText("Este CPF já está cadastrado");
            } else {
                outputLabel.setText("Houve um erro ao realizar conexão com o banco de dados");
            }

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == confirmarButton) {
            cadastrarCliente();

        } else if (e.getSource() == cancelarButton) {

        } else if (e.getSource() == sairButton) {

        }
    }

}
