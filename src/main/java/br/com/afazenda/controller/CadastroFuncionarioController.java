package br.com.afazenda.controller;

import br.com.afazenda.model.dao.EnderecoImp;
import br.com.afazenda.model.dao.FuncionarioImp;
import br.com.afazenda.model.vo.Endereco;
import br.com.afazenda.model.vo.Funcionario;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;

/**
 * Created by Natã & Givaldo Marques
 * on 27/04/17.
 */
public class CadastroFuncionarioController implements ActionListener {

    private static JFrame frame;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JButton confirmarButton;
    private JButton cancelarButton;
    private JTextField nomeTextField;
    private JTextField nascTextField;
    private JTextField emailTextField;
    private JTextField cpfTextField;
    private JTextField cepTextField;
    private JTextField lagradouroTextField;
    private JTextField numeroTextField;
    private JTextField cidadeTextField;
    private JTextField bairroTextField;
    private JTextField telefoneTextField;
    private JComboBox estadoComboBox;
    private JComboBox sexoComboBox;
    private JComboBox cargoComboBox;
    private JPanel MainPainel;
    private JLabel nomeUsuarioAtualLabel;
    private JLabel outputLabel;
    private JPanel MainPanel;

    public CadastroFuncionarioController() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        sairButton.addActionListener(this);
        confirmarButton.addActionListener(this);
        cancelarButton.addActionListener(this);

    }

    public void iniciarJanela() {
        JFrame frame = new JFrame("Cadastrar Funcionario");
        frame.setContentPane(new CadastroFuncionarioController().MainPainel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    private boolean validarCampos() {

        //necessário para a quebra de linha
        outputLabel.setText("<html>");

        boolean r = true;

        //nenhum campo pode estar vazaio
        if (nomeTextField.getText().trim().isEmpty() ||
                nascTextField.getText().trim().isEmpty() ||
                emailTextField.getText().trim().isEmpty() ||
                cpfTextField.getText().trim().isEmpty() ||
                cepTextField.getText().trim().isEmpty() ||
                lagradouroTextField.getText().trim().isEmpty() ||
                numeroTextField.getText().trim().isEmpty() ||
                cidadeTextField.getText().trim().isEmpty() ||
                telefoneTextField.getText().trim().isEmpty() ||
                bairroTextField.getText().trim().isEmpty() ||
                estadoComboBox.getSelectedIndex() == 0 ||
                //cargoComboBox.getSelectedIndex() == 0 || nao cadastrado ainda
                sexoComboBox.getSelectedIndex() == 0) {

            outputLabel.setText("Todos os campos são obrigatórios");
            return false;

        }

        String nome = nomeTextField.getText();
        if (!RegexMatches.isNome(nome)) {
            outputLabel.setText(outputLabel.getText() + "<br>Nome inválido");
        }

        String email = emailTextField.getText();
        if (!RegexMatches.isEmail(email)) {
            outputLabel.setText(outputLabel.getText() + "<br>Email inválido. Ex: " +
                    "user@dominio.com");
            r = false;
        }

        String cpf = cpfTextField.getText();
        if (!RegexMatches.isCPF(cpf)) {
            outputLabel.setText(outputLabel.getText() + "<br>CPF inválido. Ex: " +
                    "012.331.442-21");
            r = false;
        }

        String cep = cepTextField.getText();
        if (!RegexMatches.isCEP(cep)) {
            outputLabel.setText(outputLabel.getText() + "<br>CEP inválido. Ex: " +
                    "276732-820");
            r = false;
        }

        String nasc = nascTextField.getText();
        if (!RegexMatches.isDataNascimento(nasc)) {
            outputLabel.setText(outputLabel.getText() + "<br>Data inválida. Ex: " +
                    "01/12/1999");
            r = false;
        } else {
            int anoAtual = Calendar.getInstance().get(Calendar.YEAR);
            int anoEscolhido = Integer.parseInt(nasc.split("/")[2]);
            int idade = anoAtual - anoEscolhido;

            if (idade < 11) {
                outputLabel.setText(outputLabel.getText() + "<br>Ano escolhido inválido");
            }
        }

        String numeroCasa = numeroTextField.getText();
        if (!RegexMatches.isNumber(numeroCasa)) {
            outputLabel.setText(outputLabel.getText() + "<br>Número da casa inválido");

        }

        //necessário para a quebra de linha
        outputLabel.setText(outputLabel.getText() + "</html>");

        return r;
    }

    private void cadastrarFuncionario() {
        if (!validarCampos()) return;

        try {

            Endereco endereco = new Endereco(
                    lagradouroTextField.getText(),
                    Integer.parseInt(numeroTextField.getText()),
                    bairroTextField.getText(),
                    cidadeTextField.getText(),
                    estadoComboBox.getSelectedItem().toString(),
                    cepTextField.getText());
            EnderecoImp NovoEnderecoImp = new EnderecoImp(ConnectionFactory.getConnection());
            FuncionarioImp funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
            Funcionario funcionario = new Funcionario();
            funcionario.setEndereco(NovoEnderecoImp.insereEndereco(endereco));
            funcionario.setName(nomeTextField.getText());
            funcionario.setEmail(emailTextField.getText());
            funcionario.setTelefone(telefoneTextField.getText());
            funcionario.setCpf(cpfTextField.getText());
            funcionario.setNascimento(nascTextField.getText());
            funcionario.setEndereco(1);
            funcionario.setAtivo(true);

            funcionarioImp.cadastrarFuncionario(funcionario);

            outputLabel.setText("Funcionário cadastrado com sucesso");

        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                outputLabel.setText("Este CPF já está cadastrado");
            } else {
                outputLabel.setText("Houve um erro ao realizar conexão com o banco de dados");
            }
        }


    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == confirmarButton) {
            cadastrarFuncionario();

        } else if (e.getSource() == cancelarButton) {
            new PesquisarFuncionariosController().iniciarJanela();
            frame.dispose();

        } else if (e.getSource() == sairButton) {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        }

    }

}
