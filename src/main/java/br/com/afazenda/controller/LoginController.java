package br.com.afazenda.controller;

import br.com.afazenda.model.dao.FuncionarioImp;
import br.com.afazenda.model.interfacedao.FuncionarioDao;
import br.com.afazenda.model.vo.Funcionario;
import br.com.afazenda.view.MenuPrincipal;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by Natã
 * on 27/04/17.
 */
public class LoginController implements ActionListener {
    private static JFrame frame;
    /**
     *
     */
    private FuncionarioImp funcionarioImp;
    private JTextField cpfField;
    private JButton entrarButton;
    private JPanel MainPanel;
    private JLabel infoLabel;

    public LoginController() throws SQLException {
        entrarButton.addActionListener(this);

        funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
    }

    public void iniciarJanela() {

        try {
            frame = new JFrame("Login de funcionário");
            frame.setContentPane(new LoginController().MainPanel);
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);

            cpfField.setText("");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private boolean validaCampos(String cpf) {


        if (cpf.equals("")) {
            System.out.println("CPF vazio");
            infoLabel.setText("Informe seu CPF");
            return false;
        }

        if (!RegexMatches.isCPF(cpf)) {
            System.out.println("CPF mal formatado");
            infoLabel.setText("Formato de CPF inválido. Ex: 123.456.789-10");
            return false;
        }

        return true;
    }

    public boolean fazerLogin(FuncionarioDao funcionarioDao, String cpfFuncionario) {
        boolean retorno = false;
        //FuncionarioDao funcionarioDao;
        //FuncionarioImp funcionarioImp;

        if (!validaCampos(cpfFuncionario)) {

            return false;
        }

        try {
            //funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
            Funcionario funcionario = funcionarioDao.buscarFuncionario(cpfFuncionario);


            if (funcionario == null) {
                System.out.println("CPF não encontrado");
                infoLabel.setText("CPF não encontrado");
                retorno = false;

            } else {
                System.out.println("Login realizado");
                infoLabel.setText("Login realizado com sucesso");

                //salva o funcionario logado em um singleton
                UsuarioAtual.getInstance().setFuncionarioAtual(funcionario);

                new MenuPrincipal().iniciarJanela();
                retorno = true;
            }

        } catch (SQLException e1) {
            infoLabel.setText("Houve um erro ao realizar conexão com o banco de dados");
            e1.printStackTrace();
        }
        return retorno;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == entrarButton) {
            if (fazerLogin(funcionarioImp, cpfField.getText())) {
                frame.dispose();
            }
        }
    }
}
