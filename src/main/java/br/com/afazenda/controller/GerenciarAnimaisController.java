package br.com.afazenda.controller;

import br.com.afazenda.model.dao.AnimalImp;
import br.com.afazenda.model.dao.EspecieImp;
import br.com.afazenda.model.vo.Animal;
import br.com.afazenda.model.vo.Especie;
import br.com.afazenda.view.MenuAnimais;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on 02/05/17.
 */
public class GerenciarAnimaisController {
    private static JFrame frame;
    private JPanel MainPanel;
    private JButton configuraçõesButton;
    private JButton sairButton;
    private JButton adicionarButton;
    private JList list1;
    private JButton deletarButton1;
    private JButton voltarButton;
    private JComboBox especieComboBox;
    private JLabel nomeUsuarioAtualLabel;
    private JLabel outputLabel;
    private ArrayList<Especie> especies;
    private ArrayList<Animal> animais;
    private EspecieImp especieImp = new EspecieImp(ConnectionFactory.getConnection());
    private AnimalImp animalImp = new AnimalImp(ConnectionFactory.getConnection());

    public GerenciarAnimaisController() {
        preencherCombo();
        initLista();

        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());

        voltarButton.addActionListener(actionEvent -> {
            new MenuAnimais().iniciarJanela();
            frame.dispose();
        });
        sairButton.addActionListener(actionEvent -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            frame.dispose();
        });

        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                cadastrarAnimal();
                initLista();
            }
        });
        deletarButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                removerAnimal();
                initLista();
            }
        });
    }


    public void iniciarJanela() throws SQLException {
        frame = new JFrame("Animal");
        frame.setContentPane(new GerenciarAnimaisController().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void cadastrarAnimal() {
        Animal animalAuxiliar = new Animal();
        Especie especieAuxiliar = new Especie();
        especieAuxiliar = especies.get(especieComboBox.getSelectedIndex());
        animalAuxiliar.setEspecie(especieAuxiliar);

        try {
            animalImp.insereAnimal(animalAuxiliar);
            outputLabel.setText("Animal cadastrado com sucesso!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void removerAnimal() {
        try {
            animalImp.removeAnimal(animais.get(list1.getSelectedIndex()));
            outputLabel.setText("Animal removido com sucesso!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void preencherCombo() {
        try {
            especies = especieImp.getEspecies();
            DefaultComboBoxModel listModel = new DefaultComboBoxModel();
            // listModel.addElement(" Nome da Cultura | Periodo Plantio | Periodo Colheita "  );
            for (Especie percorrer : especies) {
                listModel.addElement(percorrer.getName());
            }
            especieComboBox.setModel(listModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initLista() {
        try {
            animais = animalImp.getAnimais();
            DefaultListModel listModel = new DefaultListModel();
            // listModel.addElement(" Nome da Cultura | Periodo Plantio | Periodo Colheita "  );
            for (Animal percorrer : animais) {
                listModel.addElement(" ID do Animal: " + percorrer.getEspecie().getName().toUpperCase() + percorrer.getId());
            }
            list1.setModel(listModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
