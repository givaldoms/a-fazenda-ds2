package br.com.afazenda.controller;

import br.com.afazenda.model.dao.CargoImp;
import br.com.afazenda.model.dao.FuncionarioImp;
import br.com.afazenda.model.vo.Cargo;
import br.com.afazenda.model.vo.Funcionario;
import br.com.afazenda.view.MenuFuncionarios;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick e Elias Rabelo on 02/05/17.
 */
public class PesquisarFuncionariosController implements ActionListener {

    private static JFrame frame;
    private JButton configuraçõesButton;
    private JButton sairButton;
    private JTextField cpfTextField;
    private JButton buscarButton;
    private JButton listarTodosOsFuncionáriosButton;
    private JList<String> list1;
    private JButton voltarButton;
    private JComboBox<String> cargoComboBox;
    private JButton buscarButtonCargo;
    private JButton inativarButton;
    private JButton ativarButton;
    private JButton cadastrarButton;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private ArrayList<Cargo> cargoArrayList;

    public PesquisarFuncionariosController() {
        Funcionario funcionario = UsuarioAtual.getInstance().getFuncionarioAtual();

        try {
            cargoArrayList = new CargoImp(ConnectionFactory.getConnection()).listarCargos();
            for (Cargo c : cargoArrayList) {
                cargoComboBox.addItem(c.getNome());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        nomeUsuarioAtualLabel.setText(funcionario.getName());
        buscarButton.addActionListener(this);
        listarTodosOsFuncionáriosButton.addActionListener(this);
        voltarButton.addActionListener(this);
        buscarButtonCargo.addActionListener(this);
        inativarButton.addActionListener(this);
        ativarButton.addActionListener(this);
        cadastrarButton.addActionListener(this);

        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsuarioAtual().logoffFuncionario();
                try {
                    new LoginController().iniciarJanela();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                frame.dispose();
            }
        });
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MenuFuncionarios().iniciarJanela();
                frame.dispose();
            }
        });
    }


    public void iniciarJanela() {
        frame = new JFrame("Pesquisar Funcionario");
        frame.setContentPane(new PesquisarFuncionariosController().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    private void buscarFuncionario() throws SQLException {

        FuncionarioImp funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
        Funcionario funcionario = funcionarioImp.buscarFuncionario(cpfTextField.getText());
        DefaultListModel<String> modelo = new DefaultListModel<String>();
        modelo.addElement(funcionario.getName());
        list1.setModel(modelo);
    }

    private void buscarFuncionarioCargo() throws SQLException {

        String cargo = cargoArrayList.get(cargoComboBox.getSelectedIndex()).getNome();

        FuncionarioImp funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
        ArrayList<Funcionario> funcionarios = funcionarioImp.listarFuncionariosPorCargo(cargo);
        DefaultListModel<String> modelo = new DefaultListModel<>();

        for (Funcionario f : funcionarios) {
            modelo.addElement(f.getName());
        }

        list1.setModel(modelo);
    }

    private void listarTodosFuncionarios() throws SQLException {
        FuncionarioImp funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
        ArrayList<Funcionario> funcionarios = funcionarioImp.listarFuncionarios();
        DefaultListModel<String> modelo = new DefaultListModel<>();
        int d = funcionarios.size();
        Funcionario funcionario;
        for (int i = 0; i < d; i++) {
            funcionario = funcionarios.get(i);
            modelo.addElement(funcionario.getName() + " - " + funcionario.getCpf());
        }
        list1.setModel(modelo);
    }

    private void ativarFuncionario() throws SQLException {
        String cpf = list1.getSelectedValue();
        cpf = cpf.substring(cpf.lastIndexOf(' ') + 1);
        System.out.println(cpf);
        FuncionarioImp funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
        funcionarioImp.setAtivo(cpf);

    }

    private void inativarFuncionario() throws SQLException {
        String cpf = list1.getSelectedValue();
        cpf = cpf.substring(cpf.lastIndexOf(' ') + 1);
        System.out.println(cpf);
        FuncionarioImp funcionarioImp = new FuncionarioImp(ConnectionFactory.getConnection());
        funcionarioImp.setInativo(cpf);
    }

    private void cadastrarFuncionario() {
        CadastroFuncionarioController c = new CadastroFuncionarioController();
        c.iniciarJanela();
        frame.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buscarButton) {
            try {
                buscarFuncionario();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        } else if (e.getSource() == listarTodosOsFuncionáriosButton) {
            try {
                listarTodosFuncionarios();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        } else if (e.getSource() == ativarButton) {
            try {
                ativarFuncionario();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        } else if (e.getSource() == cadastrarButton) {
            cadastrarFuncionario();

        } else if (e.getSource() == inativarButton) {
            try {
                inativarFuncionario();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } else if (e.getSource() == buscarButtonCargo) {
            try {
                buscarFuncionarioCargo();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
}
