package br.com.afazenda.controller;

import br.com.afazenda.model.dao.CargoImp;
import br.com.afazenda.model.vo.Cargo;
import br.com.afazenda.view.MenuFuncionarios;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick && Givaldo Marques
 * on 02/05/17.
 */
public class PesquisarCargosController {
    private static JFrame frame;
    private JButton configuraçõesButton;
    private JButton sairButton;
    private JTextField cargoTextField;
    private JButton adicionarButton;
    private JList list1;
    private JButton apagarButton;
    private JButton voltarButton;
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton listarTodosButton;
    private JLabel outputLabel;
    private ArrayList<Cargo> cargoArrayList;
    private CargoImp cargoImp = new CargoImp(ConnectionFactory.getConnection());

    public PesquisarCargosController() {

        try {
            cargoArrayList = cargoImp.listarCargos();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        preencheLista();

        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        voltarButton.addActionListener(actionEvent -> {
            new MenuFuncionarios().iniciarJanela();
            frame.dispose();
        });

        sairButton.addActionListener(e -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });

        adicionarButton.addActionListener(e -> {
            try {
                String nomeCargo = cargoTextField.getText();
                if (RegexMatches.isNome(nomeCargo)) {
                    Cargo cargo = new Cargo();
                    cargo.setNome(nomeCargo);
                    cargoImp.cadastrarCargo(cargo);
                    cargoArrayList = cargoImp.listarCargos();
                    preencheLista();
                } else {
                    outputLabel.setText("Cargo inválido");
                }
            } catch (SQLException e1) {
                outputLabel.setText("Cargo já cadastrado");
            }

        });
        apagarButton.addActionListener(e -> {

            try {
                Cargo cargo = cargoArrayList.get(list1.getSelectedIndex());
                cargoImp.excluirCargo(cargo);
                cargoArrayList = cargoImp.listarCargos();
                preencheLista();
                outputLabel.setText("");

            } catch (SQLException e1) {
                outputLabel.setText("Erro ao excluir cargo");

            }
        });
    }

    public static void main(String[] args) {
        new PesquisarCargosController().iniciarJanela();
    }

    private void preencheLista() {
        DefaultListModel<String> modelo = new DefaultListModel<>();
        for (Cargo c : cargoArrayList) {
            modelo.addElement(c.getNome());
        }

        list1.setModel(modelo);

    }

    public void iniciarJanela() {
        frame = new JFrame("Funcionários");
        frame.setContentPane(new PesquisarCargosController().MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
