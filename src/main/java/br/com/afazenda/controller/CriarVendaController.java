package br.com.afazenda.controller;

import br.com.afazenda.model.dao.ClienteImp;
import br.com.afazenda.model.interfacedao.ClienteDao;
import br.com.afazenda.model.vo.Cliente;
import br.com.afazenda.model.vo.ItemProduto;
import br.com.afazenda.model.vo.Venda;
import br.com.afazenda.view.MenuPrincipal;
import br.com.afazenda.view.MenuVendas;
import singletons.CarrinhoSingleton;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.sql.SQLException;

/**
 * Created by patrick && Givaldo Marques
 * on 03/05/17.
 */
public class CriarVendaController {
    private static JFrame frame;
    ClienteImp clienteImp = new ClienteImp(ConnectionFactory.getConnection());
    private JPanel MainPanel;
    private JLabel nomeUsuarioAtualLabel;
    private JButton configuracoesButton;
    private JButton sairButton;
    private JTextField cpfTextField;
    private JTextField valorPagoTextField;
    private JButton cancelarButton;
    private JButton gerarNotaFiscalButton;
    private JButton removerItemButton;
    private JButton adicionarItemButton;
    private JList<String> carrinhoList;
    private JLabel precoTotalField;
    private JLabel outputLabel;
    private Cliente cliente;

    public CriarVendaController() throws SQLException {

        preencheLista();

        cancelarButton.addActionListener(actionEvent -> {
            new MenuVendas().iniciarJanela();
            frame.dispose();
        });

        adicionarItemButton.addActionListener(e -> {
            new ListarProdutosController().iniciarJanela();
            nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
            frame.dispose();
        });

        cancelarButton.addActionListener(actionEvent -> {
            new MenuPrincipal().iniciarJanela();
            frame.dispose();
        });

        sairButton.addActionListener(e -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });
        removerItemButton.addActionListener(e -> {
            int selectedPositon = carrinhoList.getSelectedIndex();
            CarrinhoSingleton.ourInstance.getVenda().getItens().remove(selectedPositon);
            preencheLista();
        });

        gerarNotaFiscalButton.addActionListener(e -> {

            try {
                String valorPago = valorPagoTextField.getText();

                Venda venda = CarrinhoSingleton.ourInstance.getVenda();
                String cpf = cpfTextField.getText();


                if (salvarVenda(clienteImp, valorPago, venda, cpf) != null) {
                    new GerarNotaFiscalController().iniciarJanela();
                }

            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        });
    }


    public Venda salvarVenda(ClienteDao clienteDao, String valorPago, Venda venda, String cpf) throws SQLException {
        cliente = clienteDao.buscarCliente(cpf);
        String mensagem = validarCampos(venda, valorPago, cpf, cliente);

        if (mensagem != null) {
            outputLabel.setText(mensagem);
            System.out.println(mensagem);
            return null;
        }

        CarrinhoSingleton.ourInstance.getVenda().setValorPago(Float.parseFloat(valorPago));
        CarrinhoSingleton.ourInstance.getVenda().setCliente(cliente);
        return CarrinhoSingleton.ourInstance.getVenda();
    }

    private String validarCampos(Venda venda, String valorPago, String cpf, Cliente cliente) {

        if (venda.getQuantidadeItens() < 1) {
            return "Lista vazia";
        }

        if (!RegexMatches.isDecimalNumber(valorPago)) {
            return ("Valor pago inválido");
        }

        if (Float.parseFloat(valorPago) < venda.getValorVenda()) {
            return ("Valor pago insuficiente");
        }

        if (!RegexMatches.isCPF(cpf)) {
            return ("CPF inválido. Ex:123.433.443-12");
        }

        if (cliente == null) {
            return ("CPF informado não pertence a nenhum cliente");
        }

        if (cliente.isInadiplente()) {
            return ("Cliente inadimplente");
        }

        return null;
    }

    private void preencheLista() {
        float precoTotal = 0;

        DefaultListModel<String> modelo = new DefaultListModel<>();
        for (ItemProduto p : CarrinhoSingleton.ourInstance.getVenda().getItens()) {
            float precoItem = p.getProduto().getPreco() * p.getQuantidade();

            //Arroz 10 x 3 = 30
            String s = p.getProduto().getNome() + " - " + p.getProduto().getPreco() + " x " + p
                    .getQuantidade() + " = " + precoItem;
            modelo.addElement(s);

            precoTotal += precoItem;
        }
        carrinhoList.setModel(modelo);
        precoTotalField.setText(String.valueOf(precoTotal));

        Cliente cliente = CarrinhoSingleton.ourInstance.getVenda().getCliente();

        cpfTextField.setText(cliente.getCpf());

    }

    public void iniciarJanela() {
        frame = new JFrame("Venda");
        try {
            frame.setContentPane(new CriarVendaController().MainPanel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
