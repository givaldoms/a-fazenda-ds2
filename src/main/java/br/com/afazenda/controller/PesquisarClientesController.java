package br.com.afazenda.controller;

import br.com.afazenda.model.dao.ClienteImp;
import br.com.afazenda.model.vo.Cliente;
import br.com.afazenda.view.MenuCliente;
import singletons.UsuarioAtual;
import utils.ConnectionFactory;
import utils.RegexMatches;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by soulplan e Elias Rabelo on 27/04/17.
 */
public class PesquisarClientesController implements ActionListener {

    private static JFrame frame;
    private JButton configuraçõesButton;
    private JButton sairButton;
    private JTextField cpfTextField;
    private JButton buscarButton;
    private JButton listarTodosClientesButton;
    private JList list1;
    private JPanel MainPanel;
    private JButton editarButton;
    private JButton voltarButton;
    private JButton cadastrarButton;
    private JLabel nomeUsuarioAtualLabel;

    public PesquisarClientesController() {
        nomeUsuarioAtualLabel.setText(UsuarioAtual.getInstance().getFuncionarioAtual().getName());
        buscarButton.addActionListener(this);
        listarTodosClientesButton.addActionListener(this);
        editarButton.addActionListener(this);
        cadastrarButton.addActionListener(this);


        sairButton.addActionListener(e -> {
            new UsuarioAtual().logoffFuncionario();
            try {
                new LoginController().iniciarJanela();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            frame.dispose();
        });
        voltarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MenuCliente().iniciarJanela();
                frame.dispose();
            }
        });
    }

    public void iniciarJanela() {
        frame = new JFrame("PesquisarClientesController");
        frame.setContentPane(new PesquisarClientesController().MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void buscarCliente(String cpf) throws SQLException {
        ClienteImp clienteImp = new ClienteImp(ConnectionFactory.getConnection());
        if (!RegexMatches.isCPF(cpf)) {
            return;
        }
        Cliente cliente = clienteImp.buscarCliente(cpf);
        DefaultListModel modelo = new DefaultListModel();
        modelo.addElement(cliente.getNome() + " - " + cliente.getCpf());
        list1.setModel(modelo);
    }

    private void listarTodosClientes() throws SQLException {
        ClienteImp clienteImp = new ClienteImp(ConnectionFactory.getConnection());
        ArrayList<Cliente> clientes = clienteImp.buscarClientes();
        DefaultListModel modelo = new DefaultListModel();
        int d = clientes.size();
        Cliente cliente;
        for (int i = 0; i < d; i++) {
            cliente = clientes.get(i);
            modelo.addElement(cliente.getNome() + " - " + cliente.getCpf());
        }
        list1.setModel(modelo);
    }

    private void cadastrarCliente() {
        CadastroClienteController c = new CadastroClienteController();
        c.iniciarJanela();
        frame.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buscarButton) {
            try {
                buscarCliente(cpfTextField.getText());
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        if (e.getSource() == listarTodosClientesButton) {
            try {
                listarTodosClientes();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        if (e.getSource() == cadastrarButton) {
            cadastrarCliente();
        }
    }


}
