/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.AnimalDao;
import br.com.afazenda.model.vo.Animal;
import br.com.afazenda.model.vo.Especie;

import java.sql.*;
import java.util.ArrayList;

/**
 * @author givaldo
 */
public class AnimalImp implements AnimalDao {

    private Connection conexao;

    public AnimalImp(Connection conexao) {
        this.conexao = conexao;

    }

    public void insereAnimal(Animal animal) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("INSERT INTO fazenda_bd" +
                ".animal(especie) VALUES (?)");
        preparedStatement.setInt(1, animal.getEspecie().getId());

        preparedStatement.execute();
    }

    public void removeAnimal(Animal animal) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("DELETE FROM fazenda_bd" +
                ".animal WHERE idanimal = (?)");
        preparedStatement.setInt(1, animal.getId());
        preparedStatement.execute();
    }

    public ArrayList<Animal> getAnimais() throws SQLException {
        ArrayList<Animal> animais = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.animal NATURAL JOIN fazenda_bd.especie WHERE especie = idespecie;";

        Statement comando = conexao.createStatement();
        ResultSet resultado = comando.executeQuery(query);

        while (resultado.next()) {

            Animal a = new Animal();

            a.setId(resultado.getInt(1));
            Especie e = new Especie();
            e.setId(resultado.getInt(2));
            e.setName(resultado.getString(4));
            e.setQuantidade(resultado.getInt(5));
            a.setEspecie(e);

            animais.add(a);
        }

        return animais;
    }

}