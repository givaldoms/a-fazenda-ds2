package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.AgriculturaDao;
import br.com.afazenda.model.vo.CulturaAgricola;
import br.com.afazenda.model.vo.Plantacao;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques && Patrick Jones
 * on 01/05/17.
 */
public class AgriculturaImp implements AgriculturaDao {
    private Connection conexao;

    public AgriculturaImp(Connection conexao) {
        this.conexao = conexao;
    }

    @Override
    public void cadastrarCulturaAgricola(CulturaAgricola culturaAgricola) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("INSERT INTO fazenda_bd" +
                ".cultura_agricola(nome, periodoplantio, periodocolheita) VALUES( ?, ?, ?)");
        preparedStatement.setString(1, culturaAgricola.getNome());
        preparedStatement.setString(2, culturaAgricola.getPeriodoPlantio());
        preparedStatement.setString(3, culturaAgricola.getPeriodoColheita());

        preparedStatement.execute();
    }

    @Override
    public void excluirCulturaAgricola(CulturaAgricola culturaAgricola) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("DELETE FROM fazenda_bd" +
                ".cultura_agricola WHERE idcultura = (?)");
        preparedStatement.setInt(1, culturaAgricola.getId());
        preparedStatement.execute();
    }

    @Override
    public void excluirPlantioDeCultura(Plantacao plantacao) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("DELETE FROM fazenda_bd" +
                ".plantio WHERE idplantio = (?)");
        preparedStatement.setInt(1, plantacao.getIdPlantacao());
        preparedStatement.execute();
    }

    @Override
    public void cadastarPlantio(Plantacao plantacao) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("INSERT INTO fazenda_bd" +
                ".plantio(cultura, dataplantacao) VALUES (?, ?)");
        preparedStatement.setInt(1, plantacao.getCultura().getId());
        preparedStatement.setString(2, String.valueOf(plantacao.getDataPlantacao()));

        preparedStatement.execute();
    }

    @Override
    public ArrayList<CulturaAgricola> listarCulturaAgricola() throws SQLException {
        ArrayList<CulturaAgricola> culturaAgricola = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.cultura_agricola;";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            CulturaAgricola c = new CulturaAgricola();
            c.setId(resultSet.getInt(1));
            c.setNome(resultSet.getString(2));
            c.setPeriodoPlantio(resultSet.getString(3));
            c.setPeriodoColheita(resultSet.getString(4));

            culturaAgricola.add(c);
        }
        return culturaAgricola;
    }

    @Override
    public ArrayList<Plantacao> listarPlantioDeCultura() throws SQLException {
        ArrayList<Plantacao> plantacao = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.plantio NATURAL JOIN fazenda_bd.cultura_agricola WHERE cultura = cultura_agricola.idcultura;";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            Plantacao p = new Plantacao();
            p.setIdPlantacao(resultSet.getInt(1));
            CulturaAgricola c = new CulturaAgricola();
            c.setId(resultSet.getInt(4));
            c.setNome(resultSet.getString(5));
            c.setPeriodoPlantio(resultSet.getString(6));
            c.setPeriodoColheita(resultSet.getString(7));
            p.setCultura(c);
            p.setDataPlantacao(resultSet.getString(3));

            plantacao.add(p);
        }
        return plantacao;
    }
}
