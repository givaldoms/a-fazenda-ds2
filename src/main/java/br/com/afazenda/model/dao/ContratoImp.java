package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.ContratoDao;
import br.com.afazenda.model.vo.Cargo;
import br.com.afazenda.model.vo.Contrato;
import br.com.afazenda.model.vo.Funcionario;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 04/05/17.
 */
public class ContratoImp implements ContratoDao {

    private Connection conexao;

    public ContratoImp(Connection conexao) {
        this.conexao = conexao;
    }

    @Override
    public void definirContrato(Contrato contrato) throws SQLException {

        String query = "INSERT INTO fazenda_bd.contrato(datainicio, datafim, ativo, " +
                "idfuncionario, idcargo) VALUES (?, ?, ?, ?, ?)";

        PreparedStatement preparedStatement = conexao.prepareStatement(query);

        preparedStatement.setString(1, contrato.getDataContratacao());
        preparedStatement.setString(2, contrato.getDataTermino());
        preparedStatement.setBoolean(3, contrato.isAtivo());
        preparedStatement.setString(4, contrato.getFuncionario().getCpf());
        preparedStatement.setInt(5, contrato.getCargo().getId());

        preparedStatement.execute();


    }

    @Override
    public void inativarContrato(Contrato contrato) throws SQLException {
        String query = "UPDATE fazenda_bd.contrato SET ativo = FALSE WHERE idfuncionario = ?";

        PreparedStatement preparedStatement = conexao.prepareStatement(query);

        preparedStatement.setString(1, contrato.getFuncionario().getCpf());
        preparedStatement.execute();

    }

    @Override
    public ArrayList<Contrato> listarContratos() throws SQLException {
        ArrayList<Contrato> contratos = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.contrato NATURAL JOIN fazenda_bd.cargo";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            Cargo cargo = new Cargo();
            cargo.setId(resultSet.getInt(1));
            cargo.setNome(resultSet.getString(7));

            Funcionario funcionario = new Funcionario();
            funcionario.setCpf(resultSet.getString(6));

            Contrato contrato = new Contrato();
            contrato.setId(resultSet.getInt(2));
            contrato.setDataContratacao(resultSet.getString(3));
            contrato.setDataTermino(resultSet.getString(4));
            contrato.setCargo(cargo);
            contrato.setFuncionario(funcionario);
            contrato.setAtivo(resultSet.getBoolean(5));

            contratos.add(contrato);

        }

        return contratos;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ArrayList<Funcionario> listarContratados() throws SQLException {

        ArrayList<Funcionario> funcionarios = new ArrayList<>();

        String query = "SELECT cpf, nome, telefone, email, idendereco, ativo, dtnascimento " +
                "FROM fazenda_bd.contrato " +
                "NATURAL JOIN fazenda_bd.funcionario WHERE ativo;";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            //preenchendo um objeto funcionário
            Funcionario f = new Funcionario();
            f.setCpf(resultSet.getString(1));
            f.setName(resultSet.getString(2));
            f.setTelefone(resultSet.getString(3));
            f.setEmail(resultSet.getString(4));
            f.setEndereco(resultSet.getInt(5));
            f.setAtivo(resultSet.getBoolean(6));
            f.setNascimento(resultSet.getString(7));
            funcionarios.add(f);
        }

        return funcionarios;
    }

    @Override
    public ArrayList<Contrato> listarContratosPorCPF(String cpf) throws SQLException {

        ArrayList<Contrato> contratos = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.contrato NATURAL JOIN fazenda_bd.cargo WHERE contrato.idfuncionario = ?";

        //String query = "SELECT * FROM fazenda_bd.contrato NATURAL JOIN fazenda_bd.cargo WHERE idfuncionario = " + cpf;

        PreparedStatement statement = conexao.prepareStatement(query);

        statement.setString(1, cpf);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {

            Cargo cargo = new Cargo();
            cargo.setId(resultSet.getInt(1));
            cargo.setNome(resultSet.getString(7));

            Funcionario funcionario = new Funcionario();
            funcionario.setCpf(resultSet.getString(6));

            Contrato contrato = new Contrato();
            contrato.setFuncionario(funcionario);
            contrato.setId(resultSet.getInt(2));
            contrato.setDataContratacao(resultSet.getString(3));
            contrato.setDataTermino(resultSet.getString(4));
            contrato.setAtivo(resultSet.getBoolean(5));
            contrato.setCargo(cargo);

            contratos.add(contrato);
        }

        return contratos;
    }
}
