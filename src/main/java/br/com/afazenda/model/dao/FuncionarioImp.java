package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.FuncionarioDao;
import br.com.afazenda.model.vo.Funcionario;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 25/04/17.
 */
public class FuncionarioImp implements FuncionarioDao {

    private Connection conexao;

    public FuncionarioImp(Connection conexao) throws SQLException {
        this.conexao = conexao;
    }

    @Override
    public void cadastrarFuncionario(Funcionario funcionario) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("INSERT INTO fazenda_bd." +
                "funcionario(cpf, nome, telefone, email, idendereco,ativo) VALUES (?, ?, ?, ?, ?,?)");

        preparedStatement.setString(1, funcionario.getCpf());
        preparedStatement.setString(2, funcionario.getName());
        preparedStatement.setString(3, funcionario.getTelefone());
        preparedStatement.setString(4, funcionario.getEmail());
        preparedStatement.setInt(5, funcionario.getEndereco());
        preparedStatement.setBoolean(6, true);

        preparedStatement.execute();

    }

    @Override
    public Funcionario buscarFuncionario(String cpfFuncionario) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("SELECT * FROM fazenda_bd.funcionario " +
                "WHERE funcionario.cpf = ?");

        preparedStatement.setString(1, cpfFuncionario);
        ResultSet resultSet = preparedStatement.executeQuery();

        //nenhum funcionário encontrado
        if (!resultSet.next()) {
            return null;
        }

        Funcionario f = new Funcionario();
        f.setCpf(resultSet.getString(1));
        f.setName(resultSet.getString(2));
        f.setTelefone(resultSet.getString(3));
        f.setEmail(resultSet.getString(4));
        f.setEndereco(resultSet.getInt(5));
        f.setAtivo(resultSet.getBoolean(6));

        return f;

    }

    @SuppressWarnings("Duplicates")
    @Override
    public ArrayList<Funcionario> listarFuncionarios() throws SQLException {

        ArrayList<Funcionario> funcionarios = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.funcionario WHERE ativo;";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            Funcionario f = new Funcionario();
            f.setCpf(resultSet.getString(1));
            f.setName(resultSet.getString(2));
            f.setTelefone(resultSet.getString(3));
            f.setEmail(resultSet.getString(4));
            f.setEndereco(resultSet.getInt(5));
            f.setAtivo(resultSet.getBoolean(6));
            f.setNascimento(resultSet.getString(7));
            funcionarios.add(f);
        }
        return funcionarios;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ArrayList<Funcionario> listarFuncionariosPorCargo(String cargo) throws SQLException {
        ArrayList<Funcionario> funcionarios = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.cargo AS ca NATURAL JOIN fazenda_bd.contrato AS co " +
                "INNER JOIN fazenda_bd.funcionario AS f ON (f.cpf = co.idfuncionario) WHERE ca.nome = ?";

        PreparedStatement preparedStatement = conexao.prepareStatement(query);

        preparedStatement.setString(1, cargo);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Funcionario funcionario = new Funcionario();
            funcionario.setCpf(resultSet.getString(7));
            funcionario.setName(resultSet.getString(9));
            funcionario.setTelefone(resultSet.getString(10));
            funcionario.setEmail(resultSet.getString(11));
            funcionario.setEndereco(resultSet.getInt(12));
            funcionario.setAtivo(resultSet.getBoolean(6));
            funcionario.setNascimento(resultSet.getString(13));
            funcionarios.add(funcionario);
        }
        return funcionarios;
    }

    @Override
    public boolean alterarTelefone(Funcionario funcionario, String novoTelefone) throws SQLException {

        PreparedStatement preparedStatement = conexao.prepareStatement("UPDATE fazenda_bd.funcionario SET telefone =" +
                " ? WHERE cpf = ?;");

        preparedStatement.setString(1, novoTelefone);
        preparedStatement.setString(2, funcionario.getCpf());

        return preparedStatement.execute();

    }

    @Override
    public boolean alterarEmail(Funcionario funcionario, String novoEmail) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("UPDATE fazenda_bd.funcionario SET email =" +
                " ? WHERE cpf = ?;");

        preparedStatement.setString(1, novoEmail);
        preparedStatement.setString(2, funcionario.getCpf());

        return preparedStatement.execute();

    }

    @Override
    public boolean alterarEndereco(Funcionario funcionario, int novoEndereco) throws SQLException {

        PreparedStatement preparedStatement = conexao.prepareStatement("UPDATE fazenda_bd.funcionario SET endereco =" +
                " ? WHERE cpf = ?;");

        preparedStatement.setInt(1, novoEndereco);
        preparedStatement.setString(2, funcionario.getCpf());

        return preparedStatement.execute();

    }

    @Override
    public void setAtivo(String cpf) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("UPDATE fazenda_bd.funcionario SET ativo=TRUE WHERE cpf = ?");
        preparedStatement.setString(1, cpf);
        preparedStatement.execute();
    }

    @Override
    public void setInativo(String cpf) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("UPDATE fazenda_bd.funcionario SET ativo=FALSE WHERE cpf = ?");
        preparedStatement.setString(1, cpf);
        preparedStatement.execute();
    }


}
