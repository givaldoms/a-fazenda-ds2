package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.ProdutoDao;
import br.com.afazenda.model.vo.Produto;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 04/05/17.
 */
public class ProdutoImp implements ProdutoDao {

    Connection conexao;

    public ProdutoImp(Connection conexao) {
        this.conexao = conexao;
    }

    @Override
    public void cadastrarProduto(Produto produto) throws SQLException {
        String query = "INSERT INTO fazenda_bd.produto(nome, preco, origem, unidadedemedida, " +
                "datarecebimento,quantidade) VALUES (?, ?, ?, ?, ?,?);";

        PreparedStatement preparedStatement = conexao.prepareStatement(query);

        preparedStatement.setString(1, produto.getNome());
        preparedStatement.setFloat(2, produto.getPreco());
        preparedStatement.setString(3, String.valueOf(produto.getOrigem()));
        preparedStatement.setString(4, produto.getMedida());
        preparedStatement.setString(5, produto.getData());
        preparedStatement.setInt(6, produto.getQuantidade());

        preparedStatement.execute();

    }

    @Override
    public void excluirProduto(Produto produto) throws SQLException {

        String query = "DELETE FROM fazenda_bd.produto WHERE idproduto = ?;";

        PreparedStatement preparedStatement = conexao.prepareStatement(query);
        preparedStatement.setInt(1, produto.getId());
        preparedStatement.execute();

    }

    @Override
    public ArrayList<Produto> listarProdutos() throws SQLException {
        ArrayList<Produto> produtos = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.produto;";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            Produto produto = new Produto();
            produto.setId(resultSet.getInt(1));
            produto.setNome(resultSet.getString(2));
            produto.setPreco(resultSet.getFloat(3));
            produto.setOrigem(resultSet.getString(4).charAt(0));
            produto.setMedida(resultSet.getString(5));
            produto.setData(resultSet.getString(6));
            produto.setQuantidade(resultSet.getInt(7));

            produtos.add(produto);
        }

        return produtos;
    }

    @Override
    public ArrayList<Produto> buscarProdutoPorNome(String nome) throws SQLException {
        ArrayList<Produto> produtos = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.produto WHERE lower(nome) LIKE '%" + nome + "%'";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            Produto produto = new Produto();
            produto.setId(resultSet.getInt(1));
            produto.setNome(resultSet.getString(2));
            produto.setPreco(resultSet.getFloat(3));
            produto.setOrigem(resultSet.getString(4).charAt(0));
            produto.setMedida(resultSet.getString(5));
            produto.setData(resultSet.getString(6));
            produto.setQuantidade(resultSet.getInt(7));

            produtos.add(produto);
        }

        return produtos;
    }

    @Override
    public void adionarItem(Produto produto, int quantidade) throws SQLException {
        String query = "UPDATE fazenda_bd.produto SET quantidade = quantidade + ? WHERE idproduto" +
                " = ?;";

        PreparedStatement preparedStatement = conexao.prepareStatement(query);
        preparedStatement.setInt(1, quantidade);
        preparedStatement.setInt(2, produto.getId());
        preparedStatement.execute();
    }

    @Override
    public void removerItem(Produto produto, int quantidade) throws SQLException {
        String query = "UPDATE fazenda_bd.produto SET quantidade = quantidade - ? WHERE idproduto" +
                " = ?;";

        PreparedStatement preparedStatement = conexao.prepareStatement(query);
        preparedStatement.setInt(1, quantidade);
        preparedStatement.setInt(2, produto.getId());
        preparedStatement.execute();

    }
}
