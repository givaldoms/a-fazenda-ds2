package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.VacinaDao;
import br.com.afazenda.model.vo.Especie;
import br.com.afazenda.model.vo.Vacina;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by gallotropo on 4/26/17.
 */
public class VacinaImp implements VacinaDao {
    private Connection conexao;

    public VacinaImp(Connection conexao) throws SQLException {
        this.conexao = conexao;
    }


    public void cadastrarVacina(Vacina vacina) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("insert into vacina(\"nome\","
                + "\"especiealvo\") VALUES (?,?);");

        preparedStatement.setString(1, vacina.getName());
        Especie especie = vacina.getEspecieAlvo();
        preparedStatement.setInt(2, especie.getId());
    }


    public ArrayList<Vacina> getVacinas() throws SQLException {
        ArrayList<Vacina> vacinas = new ArrayList<>();
        Statement comando = conexao.createStatement();
        ResultSet resultado = comando.executeQuery("SELECT id_vacina,vacina.nome, id_especie,especie.nome as" +
                " especie_nome,quantidade FROM fazenda_bd.vacina join fazenda_bd.especie on (especiealvo=id_especie);");
        while (resultado.next()) {

            Especie e = new Especie();
            e.setId(resultado.getInt("id_especie"));
            e.setName(resultado.getString("especie_nome"));
            e.setQuantidade(resultado.getInt("quantidade"));

            Vacina v = new Vacina();

            v.setId(resultado.getInt("id_vacina"));
            v.setName(resultado.getString("especie_nome"));
            v.setEspecieAlvo(e);

            vacinas.add(v);
        }
        return vacinas;
    }


}
