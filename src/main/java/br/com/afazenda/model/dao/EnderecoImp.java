package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.EnderecoDao;
import br.com.afazenda.model.vo.Endereco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by gallotropo on 5/2/17.
 */
public class EnderecoImp implements EnderecoDao {
    private Connection conexao;

    public EnderecoImp(Connection conexao) {
        this.conexao = conexao;
    }

    @Override
    public void setEndereco(Endereco endereco) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("INSERT INTO fazenda_bd.endereco(logradouro, numero, bairro, cidade, estado,cep) VALUES (?,?,?,?,?,?);");
        preparedStatement.setString(1, endereco.getLogradouro());
        preparedStatement.setInt(2, endereco.getNumero());
        preparedStatement.setString(3, endereco.getBairro());
        preparedStatement.setString(4, endereco.getCidade());
        preparedStatement.setString(5, endereco.getEstado());
        preparedStatement.setString(6, endereco.getCep());
        preparedStatement.execute();

    }

    @Override
    public Endereco getEndereco(int id_endereco) throws SQLException {

        PreparedStatement preparedStatement = conexao.prepareStatement("SELECT * FROM fazenda_bd.endereco WHERE endereco.idendereco =  ?;");

        preparedStatement.setInt(1, id_endereco);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            return null;
        }
        Endereco endereco = new Endereco();
        endereco.setLogradouro(resultSet.getString(2));
        endereco.setNumero(resultSet.getInt(3));
        endereco.setBairro(resultSet.getString(4));
        endereco.setCidade(resultSet.getString(5));
        endereco.setEstado(resultSet.getString(6));
        endereco.setCep(resultSet.getString(7));
        return endereco;
    }

    @Override
    //Verifica se o endereço já existe, se não existir cadastra novo endereço e retorna o referente idEndereço
    public int insereEndereco(Endereco endereco) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("select fazenda_bd.insere_endereco(plogradouro =>?, pnumero=>?, pbairro=> ?, pcidade => ?, pestado =>?, pcep=>?)");
        preparedStatement.setString(1, endereco.getLogradouro());
        preparedStatement.setInt(2, endereco.getNumero());
        preparedStatement.setString(3, endereco.getBairro());
        preparedStatement.setString(4, endereco.getCidade());
        preparedStatement.setString(5, endereco.getEstado());
        preparedStatement.setString(6, endereco.getCep());


        ResultSet resultSet = preparedStatement.executeQuery();

        //nenhum funcionário encontrado
        if (!resultSet.next()) {
            return 0;
        }

        int idEndereco = resultSet.getInt(1);

        return idEndereco;

    }


}
