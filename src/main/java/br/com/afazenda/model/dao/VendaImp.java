package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.VendaDao;
import br.com.afazenda.model.vo.ItemProduto;
import br.com.afazenda.model.vo.Produto;
import br.com.afazenda.model.vo.Venda;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques && Alice
 * on 01/05/17.
 */
public class VendaImp implements VendaDao {
    Connection conexao;

    public VendaImp(Connection conexao) {
        this.conexao = conexao;
    }

//    public static void main(String[] args) {
//        VendaImp vendaImp = new VendaImp(ConnectionFactory.getConnection());
//
//        Funcionario funcionario = new Funcionario("111.111.111-11", "tadeu", "32366110", "carlos@gmail.com", 0, "12/12/99", true);
//        Cliente cliente = new Cliente("222.222.222-11", "maria", "32222222", "maria@gmail.com", 1, false);
//        Produto produto = new Produto(2, 100, 10, "kg", 897, "carne", 'a', "24/08/2017");
//
//        ArrayList<ItemProduto> products = new ArrayList<>();
//        products.add(new ItemProduto(100, produto));
//        Venda v = new Venda("24/08/2017", products, funcionario, cliente, 1000, 1000);
//
//        vendaImp.insereVenda(v);
//
//    }

    @Override
    public boolean insereVenda(Venda venda) {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conexao.prepareStatement("SELECT fazenda_bd.insere_venda(pdatavenda =>?, pvalor=>?, pcliente=> ?, pfuncionario => ?) ");

            preparedStatement.setString(1, venda.getDate());
            preparedStatement.setDouble(2, venda.getValorVenda());
            preparedStatement.setString(3, venda.getCliente().getCpf());
            preparedStatement.setString(4, venda.getFuncionario().getCpf());

            ResultSet resultSet = preparedStatement.executeQuery();

            //nenhuma venda inserida
            if (!resultSet.next()) {
                return false;
            }

            int idVenda = resultSet.getInt(1);

            for (ItemProduto i : venda.getItens()) {

                PreparedStatement preparedStatement1 = conexao.prepareStatement(
                        "INSERT INTO fazenda_bd.item_venda(idproduto, idvenda, quantidade) VALUES (?,?,?)");

                preparedStatement1.setInt(2, idVenda);
                preparedStatement1.setInt(1, i.getProduto().getId());
                preparedStatement1.setInt(3, i.getQuantidade());
                preparedStatement1.execute();

                PreparedStatement preparedStatement2 = conexao.prepareStatement("UPDATE fazenda_bd.produto SET quantidade = quantidade - ?");
                preparedStatement2.setInt(1, i.getQuantidade());
                preparedStatement2.execute();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    @Override
    public ArrayList<Produto> listarProdutosVendidos() throws SQLException {
        ArrayList<Produto> produtos = new ArrayList<>();

        String query = "SELECT produto.nome FROM fazenda_bd.venda NATURAL JOIN fazenda_bd" +
                ".item_venda NATURAL JOIN fazenda_bd.produto";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            Produto produto = new Produto();
            produto.setNome(resultSet.getString(1));
            produtos.add(produto);
        }
        return produtos;
    }
}