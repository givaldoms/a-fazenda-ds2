/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.EspecieDao;
import br.com.afazenda.model.vo.Especie;

import java.sql.*;
import java.util.ArrayList;

/**
 * @author Elias Rabelo && Patrick Jones
 */
public class EspecieImp implements EspecieDao {

    Connection conexao;

    public EspecieImp(Connection conexao) {
        this.conexao = conexao;
    }

    public void insereEspecie(String nome, int quantidade) throws SQLException {
        String query = "INSERT INTO fazenda_bd.especie(nome, quantidade) VALUES (?,?)";

        PreparedStatement comando = conexao.prepareStatement(query);
        comando.setString(1, nome);
        comando.setInt(2, quantidade);

        comando.execute();
    }

    public void insereProdutoGerado(String nomeProduto, int idEspecie) throws SQLException {
        Statement comando = conexao.createStatement();
        comando.executeQuery("INSERT INTO produtosgerados(\"id_especie\",\"nome\") VALUES (" + idEspecie + ",'" + nomeProduto + "');");
    }

    public int getIdEspecie(String especieNome) throws SQLException {
        Statement comando = conexao.createStatement();
        ResultSet resultado = comando.executeQuery("SELECT id_especie FROM fazenda_bd.especie WHERE nome='" + especieNome + "';");
        try {
            resultado.next();
            return resultado.getInt(1);
        } catch (Exception ex) {
            return 0;
        }
    }

    public ArrayList<Especie> getEspecies() throws SQLException {
        ArrayList<Especie> especies = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.especie;";

        Statement comando = conexao.createStatement();
        ResultSet resultado = comando.executeQuery(query);

        while (resultado.next()) {

            Especie especie = new Especie();

            System.out.println(resultado.getString(2));

            especie.setId(resultado.getInt(1));
            especie.setName(resultado.getString(2));
            especie.setQuantidade(resultado.getInt(3));

            especies.add(especie);
        }

        return especies;
    }

    public void excluirEspecie(Especie especie) throws SQLException {

        String query = "DELETE FROM fazenda_bd.especie WHERE idespecie = ?;";

        PreparedStatement preparedStatement = conexao.prepareStatement(query);
        preparedStatement.setInt(1, especie.getId());
        preparedStatement.execute();

    }


}
