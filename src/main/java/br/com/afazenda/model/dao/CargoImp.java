package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.CargoDao;
import br.com.afazenda.model.vo.Cargo;

import java.sql.*;
import java.util.ArrayList;


/**
 * Created by Givaldo Marques
 * on 01/05/17.
 */
public class CargoImp implements CargoDao {
    private Connection conexao = null;

    public CargoImp(Connection conexao) {
        this.conexao = conexao;
    }

    @Override
    public void cadastrarCargo(Cargo cargo) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("INSERT INTO fazenda_bd" +
                ".cargo(nome) VALUES (?)");
        preparedStatement.setString(1, cargo.getNome());

        preparedStatement.execute();
    }

    @Override
    public void excluirCargo(Cargo cargo) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("DELETE FROM fazenda_bd" +
                ".cargo WHERE nome = ?");
        preparedStatement.setString(1, cargo.getNome());

        preparedStatement.execute();
    }

    @Override
    public ArrayList<Cargo> listarCargos() throws SQLException {
        ArrayList<Cargo> cargosList = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.cargo;";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            Cargo c = new Cargo();
            c.setNome(resultSet.getString(2));
            c.setId(resultSet.getInt(1));
            cargosList.add(c);
        }

        return cargosList;
    }

}
