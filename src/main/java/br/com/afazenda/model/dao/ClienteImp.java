package br.com.afazenda.model.dao;

import br.com.afazenda.model.interfacedao.ClienteDao;
import br.com.afazenda.model.vo.Cliente;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 25/04/17.
 */
public class ClienteImp implements ClienteDao {

    private Connection conexao;

    public ClienteImp(Connection conexao) throws SQLException {
        this.conexao = conexao;
    }


    @Override
    public void cadastrarCliente(Cliente cliente) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("INSERT INTO fazenda_bd.cliente(cpf, nome, telefone, email, idendereco) VALUES (?,?,?,?,?)"
        );

        preparedStatement.setString(1, cliente.getCpf());
        preparedStatement.setString(2, cliente.getNome());
        preparedStatement.setString(3, cliente.getTelefone());
        preparedStatement.setString(4, cliente.getEmail());
        preparedStatement.setInt(5, cliente.getEndereco());
        preparedStatement.execute();
    }

    @Override
    public ArrayList<Cliente> buscarClientes() throws SQLException {
        ArrayList<Cliente> clientesList = new ArrayList<>();

        String query = "SELECT * FROM fazenda_bd.cliente;";

        Statement statement = conexao.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            Cliente c = new Cliente();
            c.setCPF(resultSet.getString(1));
            c.setNome(resultSet.getString(2));
            c.setTelefone(resultSet.getString(3));
            c.setEndereco(resultSet.getInt(4));
            c.setEmail(resultSet.getString(5));

            clientesList.add(c);
        }

        return clientesList;
    }


    @Override
    public Cliente buscarCliente(String cpfClinte) throws SQLException {
        PreparedStatement preparedStatement = conexao.prepareStatement("SELECT * FROM fazenda_bd.cliente " +
                "WHERE cliente.cpf = ?");

        preparedStatement.setString(1, cpfClinte);
        ResultSet resultSet = preparedStatement.executeQuery();

        //nenhum funcionário encontrado
        if (!resultSet.next()) {
            return null;
        }

        Cliente c = new Cliente();
        c.setCPF(resultSet.getString(1));
        c.setNome(resultSet.getString(2));
        c.setTelefone(resultSet.getString(3));
        c.setEndereco(resultSet.getInt(4));
        c.setEmail(resultSet.getString(5));
        return c;
    }

    @Override
    public void editarCliente(Cliente cliente, String cpf) throws SQLException {
        if (cliente.getEndereco() != 0) {
            PreparedStatement preparedStatement = conexao.prepareStatement("UPDATE fazenda_bd.cliente SET cpf=?,nome =?,telefone=?,idendereco=?,email=? WHERE cpf=?;");
            preparedStatement.setString(1, cliente.getCpf());
            preparedStatement.setString(2, cliente.getNome());
            preparedStatement.setString(3, cliente.getTelefone());
            preparedStatement.setInt(4, cliente.getEndereco());
            preparedStatement.setString(5, cliente.getEmail());
            preparedStatement.setString(6, cpf);
            preparedStatement.execute();
        } else {
            PreparedStatement preparedStatement = conexao.prepareStatement("UPDATE fazenda_bd.cliente SET cpf=?,nome =?,telefone=?,email=? WHERE cpf=?;");
            preparedStatement.setString(1, cliente.getCpf());
            preparedStatement.setString(2, cliente.getNome());
            preparedStatement.setString(3, cliente.getTelefone());
            preparedStatement.setString(4, cliente.getEmail());
            preparedStatement.setString(5, cpf);
            preparedStatement.execute();
        }

    }

    /*public static  void main(String Args[]) throws SQLException{
        //Cliente n = new Cliente("1010",'f',"elias","9999","elias@elias",true,"rua",false);
        ClienteImp a = new ClienteImp(ConnectionFactory.getConnection());
        /*Cliente n = new Cliente();
        n.setCPF("13");
        n.setEndereco(1);
        n.setInadiplente(false);
        n.setAtivo(true);
        n.setTelefone("jkjn,dn");
        n.setEmail("elias@elias");
        n.setNome("elias");
        a.cadastrarCliente(n);
        Cliente n = a.buscarCliente("13");


    }*/
}
