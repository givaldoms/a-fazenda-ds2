package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Funcionario;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 25/04/17.
 */
public interface FuncionarioDao {

    void cadastrarFuncionario(Funcionario funcionario) throws SQLException;

    /**
     * @param cpfFuncionario cpf do funcionário a ser autenticado
     * @return retorna null caso não seja encontrado e retorna Funcionário
     * caso seja encontrado
     */
    Funcionario buscarFuncionario(String cpfFuncionario) throws SQLException;

    boolean alterarTelefone(Funcionario funcionario, String novoTelefone) throws SQLException;

    boolean alterarEmail(Funcionario funcionario, String novoEmail) throws SQLException;

    boolean alterarEndereco(Funcionario funcionario, int novoEndereco) throws SQLException;

    void setAtivo(String cpf) throws SQLException;

    void setInativo(String cpf) throws SQLException;

    ArrayList<Funcionario> listarFuncionarios() throws SQLException;

    ArrayList<Funcionario> listarFuncionariosPorCargo(String cargo) throws SQLException;


}
