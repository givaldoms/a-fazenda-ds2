package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Cliente;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by givaldo on 25/04/17.
 */
public interface ClienteDao {

    void cadastrarCliente(Cliente cliente) throws SQLException;

    ArrayList<Cliente> buscarClientes() throws SQLException;

    Cliente buscarCliente(String cpfClinte) throws SQLException;

    void editarCliente(Cliente cliente, String cpf) throws SQLException;
}
