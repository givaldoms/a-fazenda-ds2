package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Animal;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 25/04/17.
 */
public interface AnimalDao {

    void insereAnimal(Animal animal) throws SQLException;

    void removeAnimal(Animal animal) throws SQLException;

    ArrayList<Animal> getAnimais() throws SQLException;
}
