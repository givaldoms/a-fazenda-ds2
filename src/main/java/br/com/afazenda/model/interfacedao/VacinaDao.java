package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Vacina;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by gallotropo on 4/26/17.
 */
public interface VacinaDao {

    void cadastrarVacina(Vacina vacina) throws SQLException;

    ArrayList<Vacina> getVacinas() throws SQLException;
}
