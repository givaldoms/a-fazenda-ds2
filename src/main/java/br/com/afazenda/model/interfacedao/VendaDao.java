package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Produto;
import br.com.afazenda.model.vo.Venda;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 01/05/17.
 */
public interface VendaDao {

    boolean insereVenda(Venda venda) throws SQLException;

    ArrayList<Produto> listarProdutosVendidos() throws SQLException;

}
