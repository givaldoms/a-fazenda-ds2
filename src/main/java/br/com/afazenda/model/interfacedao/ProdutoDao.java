package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Produto;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 04/05/17.
 */
public interface ProdutoDao {

    void cadastrarProduto(Produto produto) throws SQLException;

    void excluirProduto(Produto produto) throws SQLException;

    ArrayList<Produto> listarProdutos() throws SQLException;

    ArrayList<Produto> buscarProdutoPorNome(String nome) throws SQLException;

    void adionarItem(Produto produto, int quantidade) throws SQLException;

    void removerItem(Produto produto, int quantidade) throws SQLException;

}
