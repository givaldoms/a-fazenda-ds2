package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Especie;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques && Patrick Jones
 * on 25/04/17.
 */
public interface EspecieDao {

    void insereEspecie(String nome, int quantidade) throws SQLException;

    void excluirEspecie(Especie especie) throws SQLException;

    void insereProdutoGerado(String nomeProduto, int idEspecie) throws SQLException;

    int getIdEspecie(String especieNome) throws SQLException;

    ArrayList<Especie> getEspecies() throws SQLException;

    //public ArrayList<String> getProdutosGerados(int id) throws SQLException;


}
