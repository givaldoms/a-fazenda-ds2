package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Contrato;
import br.com.afazenda.model.vo.Funcionario;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 04/05/17.
 */
public interface ContratoDao {

    void definirContrato(Contrato contrato) throws SQLException;

    void inativarContrato(Contrato contrato) throws SQLException;

    ArrayList<Contrato> listarContratos() throws SQLException;

    ArrayList<Funcionario> listarContratados() throws SQLException;

    ArrayList<Contrato> listarContratosPorCPF(String cpf) throws SQLException;


}
