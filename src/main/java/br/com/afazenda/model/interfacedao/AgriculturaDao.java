package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.CulturaAgricola;
import br.com.afazenda.model.vo.Plantacao;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques && Patrick Jones
 * on 01/05/17.
 */
public interface AgriculturaDao {

    void cadastrarCulturaAgricola(CulturaAgricola culturaAgricola) throws SQLException;

    void excluirCulturaAgricola(CulturaAgricola culturaAgricola) throws SQLException;

    void cadastarPlantio(Plantacao plantacao) throws SQLException;

    ArrayList<CulturaAgricola> listarCulturaAgricola() throws SQLException;

    ArrayList<Plantacao> listarPlantioDeCultura() throws SQLException;

    void excluirPlantioDeCultura(Plantacao plantacao) throws SQLException;

}
