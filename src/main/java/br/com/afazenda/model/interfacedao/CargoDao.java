package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Cargo;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Givaldo Marques
 * on 01/05/17.
 */
public interface CargoDao {

    void cadastrarCargo(Cargo cargo) throws SQLException;

    void excluirCargo(Cargo cargo) throws SQLException;

    ArrayList<Cargo> listarCargos() throws SQLException;
}
