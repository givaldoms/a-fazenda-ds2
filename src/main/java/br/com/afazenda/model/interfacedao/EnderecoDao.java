package br.com.afazenda.model.interfacedao;

import br.com.afazenda.model.vo.Endereco;

import java.sql.SQLException;

/**
 * Created by gallotropo on 4/27/17
 */
public interface EnderecoDao {
    void setEndereco(Endereco endereco) throws SQLException;

    Endereco getEndereco(int id_endereco) throws SQLException;

    int insereEndereco(Endereco endereco) throws SQLException;

}
