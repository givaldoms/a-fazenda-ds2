package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class Vacinacao {

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
