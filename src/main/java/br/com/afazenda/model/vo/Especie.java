package br.com.afazenda.model.vo;

import java.util.ArrayList;

/**
 * @author givaldo
 */
public class Especie {
    private int id;
    private String name;
    private ArrayList<String> produtosGerados;
    private int quantidade;

    public Especie(int id_especie, String nome, int quantidade) {
    }

    public Especie() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getProdutosGerados() {
        return produtosGerados;
    }

    public void setProdutosGerados(ArrayList<String> produtosGerados) {
        this.produtosGerados = produtosGerados;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }


}
