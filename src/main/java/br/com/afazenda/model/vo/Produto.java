package br.com.afazenda.model.vo;

/**
 * Created by Givaldo Marques
 * on 01/05/17.
 */
public class Produto {

    private int id;
    private float preco;
    private String medida;
    private int quantidade;
    private String nome;
    private char origem;
    private String data;

    public Produto(int id, float preco, String medida, int quantidade, String nome, char origem, String data) {
        this.id = id;
        this.preco = preco;
        this.medida = medida;
        this.quantidade = quantidade;
        this.nome = nome;
        this.origem = origem;
        this.data = data;
    }

    public Produto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public char getOrigem() {
        return origem;
    }

    public void setOrigem(char origem) {
        this.origem = origem;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
