package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class Caixa {

    private float dinheiro;

    public float getDinheiro() {
        return dinheiro;
    }

    public void setDinheiro(float dinheiro) {
        this.dinheiro = dinheiro;
    }


}
