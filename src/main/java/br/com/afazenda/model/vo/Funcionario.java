package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class Funcionario {

    private String cpf;
    private String name;
    private String telefone;
    private String email;
    private int endereco;
    private boolean ativo;
    private String nascimento;

    public Funcionario(String cpf, String name, String telefone, String email, int endereco,
                       String nascimento, Boolean ativo) {
        this.cpf = cpf;
        this.name = name;
        this.telefone = telefone;
        this.email = email;
        this.endereco = endereco;
        this.nascimento = nascimento;
    }

    public Funcionario() {

    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEndereco() {
        return endereco;
    }

    public void setEndereco(int endereco) {
        this.endereco = endereco;
    }

    public boolean isAtivo() {
        return this.ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}
