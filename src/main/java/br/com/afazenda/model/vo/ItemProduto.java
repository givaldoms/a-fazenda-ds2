package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class ItemProduto {

    private int quantidade;
    private Produto produto;

    public ItemProduto(int quantidade, Produto produto) {
        this.quantidade = quantidade;
        this.produto = produto;
    }

    public ItemProduto() {

    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
