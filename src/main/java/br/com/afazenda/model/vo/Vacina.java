package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class Vacina {
    private int id;
    private String name;
    private Especie especieAlvo;

    public Vacina(int id_vacina, String nome, Especie especie) {
        id_vacina = id_vacina;
    }

    public Vacina() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Especie getEspecieAlvo() {
        return especieAlvo;
    }

    public void setEspecieAlvo(Especie especieAlvo) {
        this.especieAlvo = especieAlvo;
    }


}
