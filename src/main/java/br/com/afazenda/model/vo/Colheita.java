package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class Colheita {

    private CulturaAgricola cultura;
    private String dataColheita;

    public CulturaAgricola getCultura() {
        return cultura;
    }

    public void setCultura(CulturaAgricola cultura) {
        this.cultura = cultura;
    }

    public String getDataColheita() {
        return dataColheita;
    }

    public void setDataColheita(String dataColheita) {
        this.dataColheita = dataColheita;
    }


}
