package br.com.afazenda.model.vo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author givaldo
 */
public class Venda {

    private String date;
    private ArrayList<ItemProduto> itens;
    private Funcionario funcionario;
    private Cliente cliente;

    private float valorPago;

    public Venda(ArrayList<ItemProduto> itens, Funcionario funcionario, Cliente cliente, float valorPago) {
        this.date = getDate();
        this.itens = itens;
        this.funcionario = funcionario;
        this.cliente = cliente;
        this.valorPago = valorPago;
    }

    public Venda() {
        this.itens = new ArrayList<>();
        this.cliente = new Cliente();
        this.funcionario = new Funcionario();
    }

    public String getDate() {
        if (date == null) {
            return new SimpleDateFormat("dd/MM/yy").format(new Date());
        } else {
            return date;
        }
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<ItemProduto> getItens() {
        return itens;
    }

    public void setItens(ArrayList<ItemProduto> itens) {
        this.itens = itens;
    }

    public int getQuantidadeItens() {
        return getItens().size();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public float getValorPago() {
        return this.valorPago;
    }

    public void setValorPago(float valorPago) {
        this.valorPago = valorPago;
    }

    public float getValorVenda() {
        float value = 0;
        for (ItemProduto i0 : itens) {
            value += (i0.getProduto().getPreco() * i0.getQuantidade());
        }

        return value;
    }

    public float getTroco() {
        return this.valorPago - this.getValorVenda();
    }

}
