package br.com.afazenda.model.vo;

/**
 * @author givaldo && Patrick Jones
 */
public class Plantacao {
    private int idPlantacao;
    private String dataPlantacao;
    private CulturaAgricola cultura;

    public String getDataPlantacao() {
        return dataPlantacao;
    }

    public void setDataPlantacao(String dataPlantacao) {
        this.dataPlantacao = dataPlantacao;
    }

    public CulturaAgricola getCultura() {
        return cultura;
    }

    public void setCultura(CulturaAgricola cultura) {
        this.cultura = cultura;
    }

    public int getIdPlantacao() {
        return idPlantacao;
    }

    public void setIdPlantacao(int idPlantacao) {
        this.idPlantacao = idPlantacao;
    }

}
