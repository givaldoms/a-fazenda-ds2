package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class Cliente {

    private String cpf;
    private String telefone;
    private String nome;
    private String email;
    private int endereco;
    private boolean inadiplente;

    public Cliente(String cpf, String nome, String telefone, String email, int endereco, boolean inadiplente) {
        this.cpf = cpf;
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.endereco = endereco;
        this.inadiplente = inadiplente;
    }

    public Cliente() {
        this.cpf = "";

    }

    public String getCpf() {
        return cpf;
    }

    public void setCPF(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEndereco() {
        return endereco;
    }

    public void setEndereco(int endereco) {

        this.endereco = endereco;
    }

    public boolean isInadiplente() {
        return inadiplente;
    }

    public void setInadiplente(boolean inadiplente) {
        this.inadiplente = inadiplente;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }


}
