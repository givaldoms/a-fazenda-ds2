package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class Contrato {

    private int id;
    private String dataContratacao;
    private String dataTermino;
    private float salario;
    private boolean ativo;
    private Cargo cargo;
    private Funcionario funcionario;

    public String getDataContratacao() {
        return dataContratacao;
    }

    public void setDataContratacao(String dataContratacao) {
        this.dataContratacao = dataContratacao;
    }

    public String getDataTermino() {
        return dataTermino;
    }

    public void setDataTermino(String dataTermino) {
        this.dataTermino = dataTermino;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean isAtivo) {
        this.ativo = isAtivo;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
