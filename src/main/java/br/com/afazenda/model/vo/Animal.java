package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class Animal {

    private int id;
    private Especie especie;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Especie getEspecie() {
        return especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }


}
