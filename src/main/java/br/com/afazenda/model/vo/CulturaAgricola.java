package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class CulturaAgricola {

    private int id;
    private String nome;
    private String periodoPlantio;
    private String periodoColheita;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPeriodoPlantio() {
        return periodoPlantio;
    }

    public void setPeriodoPlantio(String periodoPlantio) {
        this.periodoPlantio = periodoPlantio;
    }

    public String getPeriodoColheita() {
        return periodoColheita;
    }

    public void setPeriodoColheita(String periodoColheita) {
        this.periodoColheita = periodoColheita;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
