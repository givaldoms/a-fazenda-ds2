package br.com.afazenda.model.vo;

/**
 * @author givaldo
 */
public class NotaFiscal {

    private Venda venda;
    private float imposto;

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public float getImposto() {
        return imposto;
    }

    public void setImposto(float imposto) {
        this.imposto = imposto;
    }


}
