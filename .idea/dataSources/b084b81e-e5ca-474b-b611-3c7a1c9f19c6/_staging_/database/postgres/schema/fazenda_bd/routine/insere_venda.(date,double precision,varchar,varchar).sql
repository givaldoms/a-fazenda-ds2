create function fazenda_bd.insere_venda(pdatavenda VARCHAR, pvalor double precision, pcliente character varying, pfuncionario character varying) returns integer
LANGUAGE plpgsql
AS $$
DECLARE
  vidVenda INTEGER;

BEGIN
  SELECT max(idvenda) + 1
  INTO vidVenda
  FROM fazenda_bd.venda;

  IF (vidVenda IS NULL)
  THEN
    vidVenda := 1;
  ELSE
    vidVenda := vidVenda + 1;
  END IF;

  INSERT INTO fazenda_bd.venda (idvenda,
                                datavenda,
                                valor,
                                cliente,
                                funcionario)
  VALUES (vidVenda,
          pdatavenda,
          pvalor,
          pcliente,
          pfuncionario);
  RETURN vidVenda;
END;
$$;
