CREATE SCHEMA fazenda_bd;

SET search_path TO Fazenda_bd;

CREATE TABLE IF NOT EXISTS especie (
  idespecie  SERIAL PRIMARY KEY,
  nome       VARCHAR(45) NOT NULL,
  quantidade INT         NOT NULL -- lembrar da lista de produtos que é uma classe separada
);


CREATE TABLE IF NOT EXISTS endereco (
  idendereco SERIAL PRIMARY KEY,
  logradouro VARCHAR(45) NOT NULL,
  numero     INT         NOT NULL,
  bairro     VARCHAR(45) NOT NULL,
  cidade     VARCHAR(45) NOT NULL,
  estado     VARCHAR(2)  NOT NULL,
  cep        VARCHAR(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS cliente (
  cpf          VARCHAR(14) PRIMARY KEY,
  nome         VARCHAR(100) NOT NULL,
  telefone     VARCHAR(20)  NOT NULL,
  idendereco   INT          NOT NULL,
  email        VARCHAR(100),
  dtnascimento VARCHAR(10),
  CONSTRAINT fk_endereco
  FOREIGN KEY (idendereco)
  REFERENCES endereco (idendereco)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS funcionario (
  cpf          VARCHAR(14) PRIMARY KEY,
  nome         VARCHAR(100) NOT NULL,
  telefone     VARCHAR(20),
  email        VARCHAR(100),
  idendereco   INT          NOT NULL,
  ativo        BOOLEAN,
  dtnascimento VARCHAR(10),
  CONSTRAINT fk_endereco
  FOREIGN KEY (idendereco)
  REFERENCES endereco (idendereco)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS cargo (
  idcargo SERIAL PRIMARY KEY,
  nome    VARCHAR(45) NOT NULL
);

CREATE TABLE IF NOT EXISTS contrato (
  idcontrato    SERIAL,
  dataInicio    VARCHAR(10),
  dataFim       VARCHAR(10),
  ativo         BOOLEAN,
  idfuncionario VARCHAR(14),
  idcargo       INT NOT NULL,
  PRIMARY KEY (idcontrato, idfuncionario, idcargo),
  CONSTRAINT fk_funcionario
  FOREIGN KEY (idfuncionario)
  REFERENCES funcionario (cpf)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_cargo
  FOREIGN KEY (idcargo)
  REFERENCES cargo (idcargo)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS venda (--- LEMBRAR DO PRODUTOS
  idvenda     SERIAL PRIMARY KEY,
  datavenda   VARCHAR(10)      NOT NULL,
  valor       DOUBLE PRECISION NOT NULL,
  cliente     VARCHAR(14)      NOT NULL,
  funcionario VARCHAR(14)      NOT NULL,
  CONSTRAINT fk_cliente
  FOREIGN KEY (cliente)
  REFERENCES cliente (cpf)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_funcionario
  FOREIGN KEY (funcionario)
  REFERENCES funcionario (cpf)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS animal (
  idanimal SERIAL PRIMARY KEY,
  especie  INT NOT NULL,
  CONSTRAINT fk_especie
  FOREIGN KEY (especie)
  REFERENCES especie (idespecie)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS produto (
  idproduto       SERIAL PRIMARY KEY,
  nome            VARCHAR(45)      NOT NULL,
  preco           DOUBLE PRECISION NOT NULL,
  origem          CHAR             NOT NULL,
  unidadedemedida VARCHAR(45),
  datarecebimento VARCHAR(10),
  quantidade      INT
);


CREATE TABLE IF NOT EXISTS cultura_agricola (
  idcultura       SERIAL PRIMARY KEY,
  nome            VARCHAR(45) NOT NULL,
  periodoplantio  VARCHAR(45) NOT NULL,
  periodocolheita VARCHAR(45) NOT NULL
);

CREATE TABLE IF NOT EXISTS plantio (
  idplantio     SERIAL PRIMARY KEY,
  cultura       INT         NOT NULL,
  dataplantacao VARCHAR(10) NOT NULL,
  CONSTRAINT fk_cultura
  FOREIGN KEY (cultura)
  REFERENCES cultura_agricola (idcultura)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE item_venda (
  iditemvenda SERIAL PRIMARY KEY NOT NULL,
  idproduto   INT                NOT NULL,
  idvenda     INT                NOT NULL,
  quantidade  INT                NOT NULL,


  FOREIGN KEY (idproduto)
  REFERENCES produto (idproduto),
  FOREIGN KEY (idvenda)
  REFERENCES venda (idvenda)

  ON UPDATE NO ACTION
  ON DELETE NO ACTION

);

-- FUNCTION: fazenda_bd.insere_endereco(character varying, integer, character varying, character varying, character varying, character varying)

-- DROP FUNCTION fazenda_bd.insere_endereco(character varying, integer, character varying, character varying, character varying, character varying);

-- criado por: Alice Rodrigues

CREATE OR REPLACE FUNCTION fazenda_bd.insere_endereco(
  plogradouro CHARACTER VARYING,
  pnumero     INTEGER,
  pbairro     CHARACTER VARYING,
  pcidade     CHARACTER VARYING,
  pestado     CHARACTER VARYING,
  pcep        CHARACTER VARYING)
  RETURNS INTEGER
LANGUAGE 'plpgsql'
COST 100
VOLATILE
AS $function$
DECLARE
  vidEndereco INTEGER;

BEGIN
  SELECT idendereco
  INTO vidEndereco
  FROM fazenda_bd.endereco
  WHERE UPPER(logradouro) = UPPER(plogradouro) AND
        numero = pnumero AND
        UPPER(bairro) = UPPER(pbairro) AND
        UPPER(cidade) = UPPER(pcidade) AND
        UPPER(estado) = UPPER(pestado) AND
        cep = pcep;
  IF (vidEndereco IS NULL)
  THEN

    SELECT max(idendereco)
    INTO vidEndereco
    FROM fazenda_bd.endereco;
    IF (vidEndereco IS NULL)
    THEN
      vidEndereco := 1;
    ELSE
      vidEndereco := vidEndereco + 1;
    END IF;

    INSERT INTO fazenda_bd.endereco (idendereco,
                                     logradouro,
                                     numero,
                                     bairro,
                                     cidade,
                                     estado,
                                     cep)
    VALUES (vidEndereco,
            plogradouro,
            pnumero,
            pbairro,
            pcidade,
            pestado,
            pcep);
  END IF;
  RETURN vidEndereco;
END;

$function$;

ALTER FUNCTION fazenda_bd.insere_endereco( CHARACTER VARYING, INTEGER, CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING )
OWNER TO postgres;

-- FUNCTION: fazenda_bd.insere_venda(date, double precision, character varying, character varying)

-- DROP FUNCTION fazenda_bd.insere_venda(date, double precision, character varying, character varying);

CREATE OR REPLACE FUNCTION fazenda_bd.insere_venda(
  pdatavenda   VARCHAR,
  pvalor       DOUBLE PRECISION,
  pcliente     CHARACTER VARYING,
  pfuncionario CHARACTER VARYING)
  RETURNS INTEGER
LANGUAGE 'plpgsql'
COST 100
VOLATILE
AS $function$

DECLARE
  vidVenda INTEGER;

BEGIN
  SELECT max(idvenda) + 1
  INTO vidVenda
  FROM fazenda_bd.venda;

  IF (vidVenda IS NULL)
  THEN
    vidVenda := 1;
  ELSE
    vidVenda := vidVenda + 1;
  END IF;

  INSERT INTO fazenda_bd.venda (idvenda,
                                datavenda,
                                valor,
                                cliente,
                                funcionario)
  VALUES (vidVenda,
          pdatavenda,
          pvalor,
          pcliente,
          pfuncionario);
  RETURN vidVenda;
END;

$function$;

ALTER FUNCTION fazenda_bd.insere_venda( VARCHAR(), DOUBLE PRECISION, CHARACTER VARYING, CHARACTER VARYING )
OWNER TO postgres;


INSERT INTO endereco VALUES (1, 'Rua 15', 50, 'Jabotiana', 'aracaju', 'SE', '49095000');
INSERT INTO funcionario VALUES ('111.111.111-11', 'FELIPE FLORENCIO', 'OK', 'OK', 1, TRUE, '25/11/92');